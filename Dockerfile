FROM node:22-alpine3.19 as builder-frontend

WORKDIR /app
COPY . package*.json ./
RUN npm install
COPY . ./
RUN npm run build

FROM nginx:alpine

COPY --from=builder-frontend /app/dist /usr/share/nginx/html/

RUN echo -e "#!/bin/sh\n/usr/local/bin/server &\nnginx -g 'daemon off;'" > /usr/local/bin/start.sh && chmod +x /usr/local/bin/start.sh

EXPOSE 443

# Запуск серверов
ENTRYPOINT ["nginx", "-g", "daemon off;"]