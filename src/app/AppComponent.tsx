import {Pages} from "@pages";
import {FloatingAlert} from "@shared/floatingAlert";
import {router} from "@shared/routing";
import {RouterProvider} from "atomic-router-react";
import {useEffect} from "react";

export const AppComponent = () => {

    useEffect(() => {
        ["dragover", "drop"].forEach((event) => {
            document.addEventListener(event, (e) => {
                e.preventDefault()
                return false
            })
        })
    }, []);

    return (
        <RouterProvider router={router}>
            <Pages/>
            <FloatingAlert/>
        </RouterProvider>
    );
};
