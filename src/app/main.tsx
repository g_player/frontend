import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import {appStarted} from "@shared/config";
import ReactDOM from "react-dom/client";
import {AppComponent} from "./AppComponent.tsx";
import "./index.css";

appStarted();

ReactDOM.createRoot(document.getElementById("root")!).render(
    <AppComponent/>,
);
