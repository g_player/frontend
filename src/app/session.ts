import {routes} from "@shared/routing";
import {chainRoute, RouteInstance, RouteParams, RouteParamsAndQuery} from "atomic-router";
import {createEvent, createStore, sample} from "effector";
import {persist} from "effector-storage/local";
import {jwtDecode} from "jwt-decode";
import {reset} from "patronum";

type LoggedInUserInfo = {
    username: string
    role: "user" | "admin"
}

export type Tokens = {
    access_token: string
    refresh_token: string
}

enum SessionStatus {
    Anonymous,
    Authenticated
}

export const logout = createEvent();
export const tokensChanged = createEvent<Tokens>();

const $tokens = createStore<Tokens>({access_token: "", refresh_token: ""});

$tokens.on(tokensChanged, (_, tokens) => tokens);

reset({
    clock: logout,
    target: [$tokens],
});

persist({store: $tokens, key: "tokens"});


sample({
    clock: logout,
    target: routes.auth.login.open,
});

const $sessionStatus = sample({
    source: $tokens,
    fn: (tokens) => {
        if (tokens.access_token) {
            return SessionStatus.Authenticated;
        }
        return SessionStatus.Anonymous;
    },
});

export const $loggedInUserInfo = sample({
    source: $tokens,
    fn: (tokens) => {
        if (tokens.access_token) {
            return jwtDecode<LoggedInUserInfo>(tokens.access_token);
        }
        return null;
    },
});

export const $username = $loggedInUserInfo.map(state => state?.username ?? "");
export const $isAdmin = $loggedInUserInfo.map(state => state?.role === "admin");

export function chainAuthorized<Params extends RouteParams>(route: RouteInstance<Params>) {
    const sessionCheckStarted = createEvent<RouteParamsAndQuery<Params>>();
    const alreadyAuthorized = sample({
        clock: sessionCheckStarted,
        source: $sessionStatus,
        filter: (status) => status === SessionStatus.Authenticated,
    });

    sample({
        clock: sessionCheckStarted,
        source: $sessionStatus,
        filter: (status) => status !== SessionStatus.Authenticated,
        target: routes.auth.login.open,
    });

    return chainRoute({
        route,
        beforeOpen: sessionCheckStarted,
        openOn: [alreadyAuthorized],
    });
}

export function chainAnonymous<Params extends RouteParams>(route: RouteInstance<Params>) {
    const sessionCheckStarted = createEvent<RouteParamsAndQuery<Params>>();
    const alreadyAnonymous = sample({
        clock: sessionCheckStarted,
        source: $sessionStatus,
        filter: (status) => status === SessionStatus.Anonymous,
    });

    sample({
        clock: sessionCheckStarted,
        source: $sessionStatus,
        filter: (status) => status !== SessionStatus.Anonymous,
        target: routes.main.dashboard.open,
    });

    return chainRoute({
        route,
        beforeOpen: sessionCheckStarted,
        openOn: [alreadyAnonymous],
    });
}

export function chainAdmin<Params extends RouteParams>(route: RouteInstance<Params>) {
    const sessionCheckStarted = createEvent<RouteParamsAndQuery<Params>>();
    const alreadyAdmin = sample({
        clock: sessionCheckStarted,
        source: {status: $sessionStatus, isAdmin: $isAdmin},
        filter: ({status, isAdmin}) => status === SessionStatus.Authenticated && isAdmin,
    });

    sample({
        clock: sessionCheckStarted,
        source: {status: $sessionStatus, userInfo: $loggedInUserInfo},
        filter: ({status, userInfo}) => status !== SessionStatus.Authenticated || userInfo?.role !== "admin",
        target: routes.main.dashboard.open,
    });

    return chainRoute({
        route,
        beforeOpen: sessionCheckStarted,
        openOn: [alreadyAdmin],
    });
}