import {$isAdmin} from "@app/session.ts";
import {errorSnackbarOpened} from "@entities/snackbar";
import {api, BACKEND_WS_URL, DownloadsSettings} from "@shared/api";
import {attach, createEffect, createEvent, createStore, sample} from "effector";

export type Download = {
    id: number
    name: string
    speed: string
    completedPercent: number
    status: "loading" | "paused"
}

export const openDownloadsSocket = createEvent();
export const closeDownloadsSocket = createEvent();
export const getDownloads = createEvent();
const downloadsTrackUpdated = createEvent<DownloadsSettings.DownloadDTO[]>();

const openSocketFx = createEffect(() => {
    const tokens = localStorage.getItem("tokens");
    if (tokens) {
        const accessToken = JSON.parse(tokens).access_token;
        const socket = new WebSocket(`${BACKEND_WS_URL}/api/v1/torrent/track?access_token=Bearer ${accessToken}`);
        socket.onmessage = (event) => {
            const data = JSON.parse(event.data) as DownloadsSettings.DownloadDTO[];
            downloadsTrackUpdated(data.map(download => ({
                ...download,
                completedPercent: download.completion_percentage,
            })));
        };
        const interval = setInterval(() => {
            socket.send("ping");
        }, 5000);

        socket.onerror = () => {
            errorSnackbarOpened("downloadSocketError");
            clearInterval(interval);
        };

        socket.onclose = () => {
            clearInterval(interval);
        };
        return socket;
    }
    return null;
});

const closeSocketFx = createEffect((socket: WebSocket) => {
    socket.close();
});

const getDownloadsFx = attach({effect: api.downloadsSettings.getDownloadsFx});

const $downloadSocket = createStore<WebSocket | null>(null);
export const $downloadsTrackList = createStore<DownloadsSettings.DownloadDTO[] | null>(null);

$downloadSocket.reset(closeSocketFx);

$downloadsTrackList.on(downloadsTrackUpdated, (_, downloads) => downloads);

sample({
    clock: openDownloadsSocket,
    source: {downloadSocket: $downloadSocket, isAdmin: $isAdmin},
    filter: ({isAdmin, downloadSocket}) => isAdmin && downloadSocket === null,
    target: openSocketFx,
});

sample({
    source: openSocketFx.doneData,
    target: $downloadSocket,
});

sample({
    clock: closeDownloadsSocket,
    source: $downloadSocket,
    filter: (socket) => socket !== null,
    fn: (socket) => socket!,
    target: closeSocketFx,
});

sample({
    clock: getDownloads,
    target: getDownloadsFx,
});

sample({
    source: getDownloadsFx.doneData,
    fn: (res) => res.downloads.map(download => ({
        ...download,
        completedPercent: download.completion_percentage,
    })),
    target: $downloadsTrackList,
});