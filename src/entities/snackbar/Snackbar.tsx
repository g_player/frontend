import { Alert, Snackbar as MuiSnackbar } from "@mui/material";
import { useUnit } from "effector-react";
import { $snackbarMessage, $snackbarOpen, $snackbarOpenDuration, $snackbarType, snackbarClosed } from "./model.ts";

export const Snackbar = () => {

	const [
		open,
		message,
		duration,
		type] = useUnit([
		$snackbarOpen,
		$snackbarMessage,
		$snackbarOpenDuration,
		$snackbarType,
	]);


	const getMessageFromKey = () => {
		switch (message) {
		case "registrationComplete":
			return "Успешная регистрация";
		case "downloadSocketError":
			return "Ошибка получения данных о загрузках";
		}
	};

	return <MuiSnackbar
		open={open}
		onClose={() => snackbarClosed()}
		autoHideDuration={duration}
		message={type === null && getMessageFromKey()}
	>
		{type !== null ?
			<Alert
				onClose={() => snackbarClosed()}
				sx={{ width: "100%" }}
				severity={type}
				variant="filled"
			>
				{getMessageFromKey()}
			</Alert>
			: undefined}
	</MuiSnackbar>;
};