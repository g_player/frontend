import { createEvent, createStore } from "effector";
import { reset } from "patronum";

type SnackbarType = "success" | "error" | null
export type SuccessSnackbarMessageKey = "registrationComplete"
export type ErrorSnackbarMessageKey = "downloadSocketError"

export const snackbarClosed = createEvent();
export const successSnackbarOpened = createEvent<SuccessSnackbarMessageKey>();
export const errorSnackbarOpened = createEvent<ErrorSnackbarMessageKey>();

export const $snackbarOpen = createStore(false);
export const $snackbarMessage = createStore<SuccessSnackbarMessageKey | ErrorSnackbarMessageKey | null>(null);
export const $snackbarOpenDuration = createStore(3000);
export const $snackbarType = createStore<SnackbarType>(null);

reset({
	clock: snackbarClosed,
	target: [$snackbarOpen, $snackbarOpenDuration, $snackbarType, $snackbarMessage],
});


$snackbarOpen.on([successSnackbarOpened, errorSnackbarOpened], () => true);
$snackbarType.on(successSnackbarOpened, () => "success");
$snackbarType.on(errorSnackbarOpened, () => "error");
$snackbarMessage.on([successSnackbarOpened, errorSnackbarOpened], (_, message) => message);