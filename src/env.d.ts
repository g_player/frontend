interface ImportMetaEnv {
    VITE_BACKEND_SCHEMA: string
    VITE_BACKEND_HOST: string;
    VITE_BACKEND_PORT: string

    VITE_BACKEND_WS_SCHEMA: string;
}