import {api, ExternalTitle} from "@shared/api";
import {attach, createEvent, createStore, sample} from "effector";
import {reset} from "patronum";

const findExternalTitleFx = attach({effect: api.title.findExternalTitlesFx})
export const createTitleFx = attach({effect: api.title.createTitleFx})

export const addTitleModalShown = createEvent()
export const addTitleModalClosed = createEvent()
export const searchValueChanged = createEvent<string>()
export const searchStarted = createEvent()
export const externalTitleSelected = createEvent<ExternalTitle.ExternalTitleDTO>()
export const selectedCleared = createEvent()
export const newTitleCreated = createEvent()

export const $isAddTitleModalOpened = createStore(false)
export const $searchValue = createStore("")
export const $externalTitles = createStore<ExternalTitle.ExternalTitleDTO[]>([])
export const $selectedExternalTitle = createStore<ExternalTitle.ExternalTitleDTO | null>(null)

reset({
    clock: addTitleModalClosed,
    target: [$isAddTitleModalOpened, $searchValue, $selectedExternalTitle]
})

$selectedExternalTitle.reset([selectedCleared, searchStarted])

$isAddTitleModalOpened.on(addTitleModalShown, () => true)
$searchValue.on(searchValueChanged, (_, searchValue) => searchValue)
$externalTitles.on(findExternalTitleFx.doneData, (_, titles) => titles)
$selectedExternalTitle.on(externalTitleSelected, (_, title) => title)

sample({
    clock: searchStarted,
    source: $searchValue,
    filter: (searchValue) => searchValue.trim() !== "",
    fn: (searchValue) => ({name: searchValue}),
    target: findExternalTitleFx
})

sample({
    clock: newTitleCreated,
    source: $selectedExternalTitle,
    filter: (title) => title !== null,
    fn: (title) => ({external_id: title!.id}),
    target: createTitleFx
})

