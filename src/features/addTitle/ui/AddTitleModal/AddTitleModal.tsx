import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import SearchIcon from '@mui/icons-material/Search';
import {Button, IconButton, Modal, TextField} from "@shared/ui";
import {useUnit} from 'effector-react'
import {FormEvent, Fragment, useCallback} from "react";
import {
    $externalTitles,
    $isAddTitleModalOpened,
    $searchValue,
    $selectedExternalTitle,
    addTitleModalClosed,
    externalTitleSelected,
    newTitleCreated,
    searchStarted,
    searchValueChanged,
    selectedCleared
} from '../../model'

export const AddTitleModal = () => {
    const [
        titles,
        searchValue,
        isModalOpened,
        selectedTitle
    ] = useUnit([
        $externalTitles,
        $searchValue,
        $isAddTitleModalOpened,
        $selectedExternalTitle
    ])


    const handleSearchSubmit = useCallback((e: FormEvent) => {
        e.preventDefault()
        searchStarted()
    }, [])

    return <Modal opened={isModalOpened} onClose={() => addTitleModalClosed()} withCloseButton>
        <div className="max-w-2xl">
            <div className="flex  items-center pb-3 gap-3">
                {selectedTitle && <IconButton onClick={() => selectedCleared()}>
                    <ArrowBackIcon/>
                </IconButton>}
                <h1 className="text-3xl">
                    {!selectedTitle ? "Найти тайтл" : (selectedTitle.name || selectedTitle.alternative_name)}
                </h1>
            </div>
            <form onSubmit={handleSearchSubmit}>
                <TextField
                    fullWidth
                    value={searchValue}
                    onChange={(e) => searchValueChanged(e.target.value)}
                    endAdornment={
                        <IconButton type="submit">
                            <SearchIcon/>
                        </IconButton>
                    }
                />
            </form>
            {!selectedTitle && titles.length > 0 &&
                <div className="flex flex-wrap w-full max-h-[42rem] mt-3 overflow-auto">
                    {titles.map(title => <Fragment key={title.id}>
                        {title.poster_url && <div
                            className="w-1/3 flex justify-center content-center text-center flex-col cursor-pointer p-3 transition hover:bg-pc-300/50"
                            onClick={() => externalTitleSelected(title)}
                        >
                            <div className="flex justify-center content-center">
                                <img
                                    className="rounded"
                                    src={title.poster_url}
                                    alt=""
                                />
                            </div>
                            <div>
                                <h4>
                                    {title.name || title.alternative_name}
                                </h4>
                            </div>
                        </div>}
                    </Fragment>)}
                </div>}
            {selectedTitle && <div
                className="flex max-h-[42rem] mt-3 overflow-auto gap-3"
            >
                <div className="w-2/3 flex flex-col gap-3">
                    <img
                        className="rounded"
                        src={selectedTitle.poster_url}
                        alt=""
                    />
                    <Button
                        variant="contained"
                        onClick={() => newTitleCreated()}
                    >
                        Добавить
                    </Button>
                </div>
                <p className="w-2/3">{selectedTitle.description}</p>
            </div>}

        </div>
    </Modal>
}