import {createEvent, createStore} from "effector";
import {createGate} from "effector-react";
import {reset} from "patronum";

export const Gate = createGate()

export const addTorrentModalShow = createEvent()
export const addTorrentModalClosed = createEvent()

export const $isAddTorrentModalOpened = createStore(false)

$isAddTorrentModalOpened.on(addTorrentModalShow, () => true)
$isAddTorrentModalOpened.on(addTorrentModalClosed, () => false)

reset({
    clock: Gate.open,
    target: [$isAddTorrentModalOpened]
})

