import {api} from "@shared/api";
import {copyTextToClipboardFx} from "@shared/tools";
import {attach, createEvent, createStore, restore, sample} from "effector";
import {reset} from "patronum";

export const createRegistrationTokenFx = attach({effect: api.usersSettings.createRegistrationTokenFx});
const copyTokenFx = attach({effect: copyTextToClipboardFx});
const getTokensFx = attach({effect: api.usersSettings.getUsersRegistrationTokensFx});
const deleteTokenFx = attach({effect: api.usersSettings.deleteUserRegistrationTokenFx});

export const tokenGenerated = createEvent();
export const tokenCopied = createEvent<string>();
export const modalShown = createEvent();
export const modalClosed = createEvent();
export const deleteRegistrationToken = createEvent<number>();

export const $isModalOpen = createStore(false)
export const $tokenToCopy = createStore<string | null>(null);
export const $initLoading = createStore(false)

export const $tokens = restore(getTokensFx.doneData, null)

$isModalOpen.on(modalShown, () => true)
$isModalOpen.on(modalClosed, () => false)
$initLoading.on(modalShown, () => true)
$initLoading.on(getTokensFx.doneData, () => false)

reset({
    clock: modalClosed,
    target: [$tokenToCopy, $initLoading],
});

sample({
    clock: [modalShown, createRegistrationTokenFx.doneData, deleteTokenFx.doneData],
    target: getTokensFx,
})

sample({
    clock: tokenGenerated,
    target: createRegistrationTokenFx
})

sample({
    source: createRegistrationTokenFx.doneData,
    fn: (res) => res.registration_token,
    target: copyTokenFx,
});

sample({
    clock: tokenCopied,
    target: copyTokenFx
})

sample({
    clock: deleteRegistrationToken,
    target: deleteTokenFx
})
