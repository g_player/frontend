import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import DeleteIcon from '@mui/icons-material/Delete';
import {Button, IconButton, Modal} from "@shared/ui";
import {useUnit} from "effector-react";
import {
    $initLoading,
    $isModalOpen,
    $tokens,
    deleteRegistrationToken,
    modalClosed,
    tokenCopied,
    tokenGenerated
} from '../../model'

export const CreateRegistrationTokenModal = () => {
    const [tokens, initLoading, isModalOpen] = useUnit([$tokens, $initLoading, $isModalOpen])
    return <Modal opened={isModalOpen} onClose={modalClosed}>
        <div className="flex flex-col items-center min-w-80 min-h-80">
            {initLoading && <div>Loading...</div>}
            {!initLoading && <>
                {!tokens && <div>Нет никаких токенов :(</div>}
                {tokens?.free_tokens && <div>
                    <h3 className="p-3 text-2xl">Доступные токены</h3>
                    <table className="table-auto text-center overflow-auto">
                        <thead className="border-b-2 border-pc-100">
                        <tr className="p-3">
                            <td className="p-3">
                                Id
                            </td>
                            <td className="p-3">
                                Годен до...
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        {tokens?.free_tokens?.map(tokenInfo => <tr key={tokenInfo.id} className="p-3">
                            <td className="p-3">
                                {tokenInfo.id}
                            </td>
                            <td className="p-3">
                                {new Date(tokenInfo.expired_at).toLocaleDateString()}
                            </td>
                            <td className="p-3">
                                <IconButton onClick={() => deleteRegistrationToken(tokenInfo.id)}>
                                    <DeleteIcon/>
                                </IconButton>
                            </td>
                            <td className="p-3">
                                <IconButton onClick={() => tokenCopied(tokenInfo.token)}>
                                    <ContentCopyIcon/>
                                </IconButton>
                            </td>
                        </tr>)}
                        </tbody>
                    </table>
                </div>}
                {!initLoading && tokens?.occupied_tokens && <div>
                    <h3 className="p-3 text-2xl">Токены пользователей</h3>
                    <table className="table-auto text-center">
                        <thead className="border-b-2 border-pc-100">
                        <tr className="p-3">
                            <td className="p-3">Id</td>
                            <td className="p-3">Годен до...</td>
                            <td className="p-3">Имя пользователя</td>
                        </tr>
                        </thead>
                        <tbody>
                        {tokens?.occupied_tokens.map(tokenInfo => <tr key={tokenInfo.id} className="p-3">
                            <td className="p-3">{tokenInfo.id}</td>
                            <td className="p-3">{new Date(tokenInfo.expired_at).toLocaleDateString()}</td>
                            <td className="p-3">{tokenInfo.username}</td>
                        </tr>)}
                        </tbody>
                    </table>
                </div>}
                <Button
                    onClick={() => tokenGenerated()}
                    variant="contained"
                    fullWidth
                    className="mt-3"
                >
                    Создать токен
                </Button>
            </>}
        </div>
    </Modal>
}