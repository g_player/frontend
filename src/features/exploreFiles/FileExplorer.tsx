import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import FolderIcon from "@mui/icons-material/Folder";
import LocalMoviesIcon from '@mui/icons-material/LocalMovies';

import {Box, BoxProps, Button, Divider, IconButton, MenuItem, Select, styled, Tooltip, Typography} from "@mui/material";
import {ModalBase} from "@shared/ui";
import {useUnit} from "effector-react";
import {useMemo} from "react";
import {
    $canClearPath,
    $canSelectResolution,
    $currentPath,
    $fileExplorerOpenMode,
    $files,
    $filesLoading,
    $focusFile,
    $resolution,
    dirOpened,
    fileExplorerClosed,
    fileFocused,
    fileSelected,
    pathCleared,
    prevDirOpened,
    Resolution,
    resolutionChanged,
} from "./model.ts";

const formatFileName = (fileName: string): string => {
    const parts = fileName.split(".")
    const extension = parts[parts.length - 1]
    parts.pop()
    let name = parts.join(".")
    if (name.length > 15) {
        const nameEnd = name.slice(name.length - 5, name.length)
        name = name.slice(0, 15) + "..." + nameEnd
    }
    if (extension === "mkv" || extension === "mp4") {
        name = name + "." + extension
    }
    return name
}

interface FileItemProps extends BoxProps {
    focused: boolean;
}

const FileItemBox = styled(Box, {
    shouldForwardProp: (prop: PropertyKey) => prop !== "focused",
})<FileItemProps>(({focused, theme}) => ({
    cursor: "pointer",
    padding: theme.spacing(2),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    borderRadius: "3px",
    transition: theme.transitions.create(["background-color", "transform"], {
        duration: theme.transitions.duration.standard,
    }),
    ...(!focused && {
        "&:hover": {
            backgroundColor: theme.palette.grey.A700,
        },
    }),
    ...(focused && {
        backgroundColor: theme.palette.primary.dark,
    }),
}));

export const FileExplorer = () => {
    const [
        focusFile,
        mode,
        files,
        filesLoading,
        currentPath,
        canClearPath,
        resolution,
        canSelectResolution
    ] = useUnit([
        $focusFile,
        $fileExplorerOpenMode,
        $files,
        $filesLoading,
        $currentPath,
        $canClearPath,
        $resolution,
        $canSelectResolution
    ]);

    const canSelectDir = useMemo(() => mode === "fileOrDirSelect" || mode === "onlyDirSelect" && focusFile && focusFile.is_dir, [mode, focusFile]);
    const canSelectFile = useMemo(() => mode === "fileOrDirSelect" || mode === "onlyFileSelect" && focusFile && !focusFile.is_dir, [mode, focusFile]);
    const canSelect = useMemo(() => canSelectDir || canSelectFile, [canSelectDir, canSelectFile]);
    const canOpen = useMemo(() => focusFile?.is_dir, [focusFile]);

    return <ModalBase open={mode !== null} onClose={fileExplorerClosed}>
        <Box
            height={"400px"}
            width={"600px"}
            display={"flex"}
            flexDirection={"column"}
            justifyContent={"space-between"}
            overflow={"auto"}
        >
            <Box>
                <Box display={"flex"} flexDirection={"column"}>
                    <Box display={"flex"} gap={"20px"} alignItems={"center"}>
                        <Tooltip title={currentPath === "/" ? "" : "Перейти на уровень выше"} arrow placement={"top"}>
                            <IconButton onClick={() => prevDirOpened()} disabled={currentPath === "/"}>
                                <ArrowDropUpIcon/>
                            </IconButton>
                        </Tooltip>
                        <Typography>{formatFileName(currentPath) || "/"}</Typography>
                    </Box>
                    <Divider sx={{mt: "10px"}}/>
                </Box>
                {filesLoading && <div>loading</div>}
                {!filesLoading && files &&
                    <Box mt={"20px"} display={"flex"} gap={"20px"} flexWrap={"wrap"}>
                        {files.map((file, index) => <FileItemBox
                            key={file.file_name}
                            focused={focusFile?.file_name === file.file_name}
                            onClick={() => fileFocused(index)}
                            onDoubleClick={() => file.is_dir ? dirOpened() : fileSelected()}
                        >
                            {file.is_dir && <FolderIcon sx={{fontSize: "100px"}}/>}
                            {!file.is_dir && <LocalMoviesIcon sx={{fontSize: "100px"}}/>}
                            <Typography>{formatFileName(file.file_name)}</Typography>
                        </FileItemBox>)}
                    </Box>}
            </Box>
            <Box display={"flex"} justifyContent={"space-between"} alignItems={"center"}>
                {canSelectResolution && <Box>
                    <Select
                        label={"Исзодное разрешение"}
                        value={resolution}
                        onChange={(e) => resolutionChanged(e.target.value as Resolution)}
                    >
                        <MenuItem value={"360p"}>360p</MenuItem>
                        <MenuItem value={"720p"}>720p</MenuItem>
                        <MenuItem value={"1080p"}>1080p</MenuItem>
                        <MenuItem value={"2k"}>2k</MenuItem>
                        <MenuItem value={"4k"}>4k</MenuItem>
                    </Select>
                </Box>}
                <Box alignSelf={"flex-end"}>
                    {canClearPath && <Button onClick={() => pathCleared()}>Очистить путь</Button>}
                    {focusFile && canOpen && <Button onClick={() => dirOpened()}>Открыть</Button>}
                    {focusFile && canSelect && <Button onClick={() => fileSelected()}>Выбрать</Button>}
                </Box>
            </Box>
        </Box>
    </ModalBase>;
};