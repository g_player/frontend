import {api, Os} from "@shared/api";
import {attach, createEvent, createStore, restore, sample} from "effector";
import {previous, reset} from "patronum";

export type Resolution = "360p" | "720p" | "1080p" | "2k" | "4k"

const upPath = (path: string): string => {
    const pathDirs = [...path.split("/")];
    pathDirs.pop();
    return pathDirs.join("/") || "/";
};

type OpenMode = "onlyFileSelect" | "onlyDirSelect" | "fileOrDirSelect"
export type OpenParams = {
    mode: OpenMode
    path?: string
    canClearPath?: boolean
    canSelectResolution?: boolean
    dir: "torrents" | "titles"
}

const getTitlesDirFx = attach({effect: api.os.getTitlesDirFx});

export const fileExplorerOpened = createEvent<OpenParams | undefined>();
export const fileExplorerClosed = createEvent();
export const fileFocused = createEvent<number>();
export const dirOpened = createEvent();
export const prevDirOpened = createEvent();
export const fileSelected = createEvent();
export const pathCleared = createEvent();
export const resolutionChanged = createEvent<Resolution>();

const $dir = createStore<"torrents" | "titles" | null>(null)
export const $canSelectResolution = createStore(false)
export const $files = restore(getTitlesDirFx.doneData, null);
export const $filesLoading = getTitlesDirFx.pending;
export const $fileExplorerOpenMode = createStore<OpenMode | null>(null);
export const $focusFile = createStore<Os.FileDTO | null>(null);
export const $currentPath = createStore("/");
export const $canClearPath = createStore(false);
export const $selectedFile = createStore<Os.FileDTO | null>(null)
export const $resolution = createStore<Resolution>("720p")


$canSelectResolution.on(fileExplorerOpened, (_, params) => params?.canSelectResolution ?? false)
$dir.on(fileExplorerOpened, (_, params) => params?.dir ?? null)
$canClearPath.on(fileExplorerOpened, (_, params) => params?.canClearPath ?? false);
$currentPath.on(fileExplorerOpened, (_, params) => params?.path ?? "/");
$fileExplorerOpenMode.on(fileExplorerOpened, (_, params) => params?.mode ?? null);
$resolution.on(resolutionChanged, (_, resolution) => resolution)


reset({
    clock: fileExplorerClosed,
    target: [
        $focusFile,
        $currentPath,
        $files,
        $fileExplorerOpenMode,
        $canClearPath,
    ],
});

reset({
    clock: getTitlesDirFx.pending,
    target: [$focusFile],
});

sample({
    clock: fileExplorerOpened,
    source: {currentPath: $currentPath, dir: $dir},
    filter: ({dir}) => dir !== null,
    fn: ({currentPath, dir}) => ({currentPath: currentPath, dir: dir!}),
    target: getTitlesDirFx,
});

sample({
    clock: fileFocused,
    source: $files,
    filter: (files) => files !== null,
    fn: (files, selectedIndex) => files![selectedIndex],
    target: $focusFile,
});

sample({
    clock: dirOpened,
    source: $focusFile,
    filter: (focusedFile) => focusedFile !== null && focusedFile.is_dir,
    fn: (focusedFile) => focusedFile!.local_path,
    target: $currentPath,
});

sample({
    clock: dirOpened,
    source: {focusedFile: $focusFile, dir: $dir},
    filter: ({focusedFile, dir}) => focusedFile !== null && focusedFile.is_dir && dir !== null,
    fn: ({dir, focusedFile}) => ({path: focusedFile!.local_path, dir: dir!}),
    target: getTitlesDirFx,
});

sample({
    clock: prevDirOpened,
    source: {currentPath: $currentPath, filesLoading: $filesLoading},
    filter: ({currentPath, filesLoading}) => currentPath !== "/" && !filesLoading,
    fn: ({currentPath}) => upPath(currentPath!),
    target: $currentPath,
});

sample({
    clock: prevDirOpened,
    source: {currentPath: previous($currentPath), filesLoading: $filesLoading, dir: $dir},
    filter: ({currentPath, filesLoading, dir}) => currentPath !== "/" && !filesLoading && dir !== null,
    fn: ({currentPath, dir}) => ({path: upPath(currentPath!), dir: dir!}),
    target: getTitlesDirFx,
});

sample({
    clock: fileSelected,
    source: $focusFile,
    target: $selectedFile
})