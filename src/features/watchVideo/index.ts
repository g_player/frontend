export {
    $videoCurrentTimeSeconds,
    videoPaused,
    videoPlaying,
    videoCurrentTimeChanged,
    $videoDurationSeconds,
    $canSkipEpisode,
    nextEpisodeStarted,
} from './model.ts'

export {Video} from './ui/Component.tsx'