import {createEvent, createStore, sample} from "effector";
import {createGate} from "effector-react";
import {and} from "patronum";

export type StreamGateParams = {
    titleType?: "serial" | "movie";
    nextEpisodeId?: number;
};

export const Gate = createGate<StreamGateParams>();

export const videoPlaying = createEvent();
export const videoPaused = createEvent();
export const videoCurrentTimeChanged = createEvent<number>();
export const videoDurationChanged = createEvent<number>();
export const nextEpisodeStarted = createEvent();

export const $isVideoPlaying = createStore(false);
export const $videoCurrentTimeSeconds = createStore(0);
export const $videoDurationSeconds = createStore(0);
export const $canSkipEpisode = createStore(false);
export const $isSerial = Gate.state.map((state) => state.titleType === "serial");
const $nextEpisodeId = Gate.state.map((state) => state.nextEpisodeId ?? null);

$isVideoPlaying.on(videoPlaying, () => true);
$isVideoPlaying.on(videoPaused, () => false);
$videoCurrentTimeSeconds.on(videoCurrentTimeChanged, (_, time) => time);
$videoDurationSeconds.on(videoDurationChanged, (_, duration) => duration);

sample({
    clock: videoCurrentTimeChanged,
    source: $videoDurationSeconds,
    filter: and($isSerial, $nextEpisodeId),
    fn: (videoDuration, currentTime) => currentTime >= videoDuration - 20,
    target: $canSkipEpisode,
});

sample({
    clock: videoCurrentTimeChanged,
    source: {
        isSerial: $isSerial,
        nextEpisodeId: $nextEpisodeId,
        videoDuration: $videoDurationSeconds,
    },
    filter: ({isSerial, nextEpisodeId, videoDuration}, currentTime) =>
        isSerial && !!nextEpisodeId && currentTime >= videoDuration,
    target: nextEpisodeStarted,
});
