import {
    $isSerial,
    $isVideoPlaying,
    $videoCurrentTimeSeconds,
    $videoDurationSeconds,
    Gate,
    StreamGateParams,
    videoCurrentTimeChanged,
    videoDurationChanged,
    videoPaused,
    videoPlaying
} from "@features/watchVideo/model.ts";
import {EpisodeSelector} from "@features/watchVideo/ui/EpisodeSelector/EpisodeSelector.tsx";
import {ProgressBar} from "@features/watchVideo/ui/ProgressBar/ProgressBar.tsx";
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import FullscreenExitIcon from '@mui/icons-material/FullscreenExit';
import MovieIcon from '@mui/icons-material/Movie';
import PauseIcon from '@mui/icons-material/Pause';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import {BACKEND_URL, Episode} from "@shared/api";
import {IconButton} from "@shared/ui";
import classNames from "classnames";
import {useGate, useUnit} from "effector-react";
import Hls from "hls.js";
import {MouseEvent, useCallback, useEffect, useRef, useState} from "react";

// function secondsToTime(value: number): string {
//     const hours = Math.floor(value / 3600);
//     const minutes = Math.floor((value % 3600) / 60);
//     const seconds = Math.round(value % 60); // Округляем секунды до ближайшего целого числа
//
//     const hoursStr = hours.toString().padStart(2, '0');
//     const minutesStr = minutes.toString().padStart(2, '0');
//     const secondsStr = seconds.toString().padStart(2, '0');
//
//     return `${hoursStr}:${minutesStr}:${secondsStr}`;
// }

const baseUrl = `${BACKEND_URL}/api/v1/`


type VideoProps =
    {
        srcDir?: string,
        watchedTime?: number,
        additionalEpisodes?: { [key: string]: Episode.EpisodeDTO[] }
    }
    & StreamGateParams

export const Video = ({nextEpisodeId, titleType, srcDir, watchedTime, additionalEpisodes}: VideoProps) => {
    useGate(Gate, {titleType, nextEpisodeId})
    const [duration, currentTime, isVideoPlaying] = useUnit([$videoDurationSeconds, $videoCurrentTimeSeconds, $isVideoPlaying, $isSerial])
    const [isControlPanelVisible] = useState<boolean>(true)
    const [isEpisodeSelectorOpened, setIsEpisodeSelectorOpened] = useState(false)
    const [isFullScreen, setFullScreen] = useState<boolean>(false)
    const hlsRef = useRef(new Hls())
    const trackWatchIntervalRef = useRef<NodeJS.Timeout | null>(null)
    const controlPanelVisibilityTimeoutRef = useRef<NodeJS.Timeout | null>(null)
    const videoRef = useRef<HTMLVideoElement>(null)
    const containerRef = useRef<HTMLDivElement>(null)
    const episodeSelectorButtonRef = useRef<HTMLButtonElement>(null)

    const showControlPanel = useCallback(() => {
        // setControlPanelVisible(true)
        // if (controlPanelVisibilityTimeoutRef.current) {
        //     clearTimeout(controlPanelVisibilityTimeoutRef.current)
        // }
        // controlPanelVisibilityTimeoutRef.current = setTimeout(() => {
        //     setControlPanelVisible(false)
        // }, 3000)
    }, [])

    const handlePlay = useCallback(() => {
        if (videoRef.current) {
            videoRef.current.play()
            videoPlaying()
            if (!trackWatchIntervalRef.current) {
                trackWatchIntervalRef.current = setInterval(() => {
                    videoCurrentTimeChanged(videoRef.current!.currentTime)
                }, 100)
            }
        }
    }, [])

    const handlePause = useCallback(() => {
        if (videoRef.current) {
            videoRef.current.pause()
            videoPaused()
            if (trackWatchIntervalRef.current) {
                clearInterval(trackWatchIntervalRef.current)
                trackWatchIntervalRef.current = null
            }
        }
    }, [])

    useEffect(() => {
        return () => {
            if (trackWatchIntervalRef.current) {
                clearInterval(trackWatchIntervalRef.current)
            }
            if (controlPanelVisibilityTimeoutRef.current) {
                clearTimeout(controlPanelVisibilityTimeoutRef.current)
            }
        }
    }, []);

    useEffect(() => {
        if (hlsRef.current && srcDir && videoRef.current && Hls.isSupported()) {
            hlsRef.current.config.xhrSetup = (xhr, url) => {
                const tokens = localStorage.getItem("tokens")
                if (tokens !== null) {
                    const parsedTokens = JSON.parse(tokens)
                    xhr.open("GET", url.replace(baseUrl, `${baseUrl}stream?filePath=${srcDir}/`))
                    xhr.setRequestHeader("Authorization", `Bearer ${parsedTokens.access_token}`)
                }
            }
            hlsRef.current.loadSource(`${baseUrl}master.m3u8`)
            hlsRef.current.attachMedia(videoRef.current)
            hlsRef.current.on(Hls.Events.MANIFEST_PARSED, function () {
                if (videoRef.current) {
                    videoRef.current.onloadedmetadata = () => {
                        if (watchedTime) {
                            videoDurationChanged(videoRef.current!.duration)
                            videoRef.current!.currentTime = watchedTime
                            videoCurrentTimeChanged(watchedTime)
                        } else {
                            videoRef.current!.currentTime = 0
                            videoCurrentTimeChanged(0)
                        }
                        //TODO размьютить

                        // handlePlay()
                    }
                }
            })
            hlsRef.current.on(Hls.Events.FRAG_LOADING, (_, data) => {
                return data.frag.url = data.frag.url.replace(baseUrl, `${baseUrl}?filePath=${srcDir}/`)
            })
            hlsRef.current.on(Hls.Events.ERROR, (event, data) => {
                console.log(event, data)
            })
        }
    }, [watchedTime, srcDir, handlePlay]);

    const togglePlay = useCallback(() => {
        showControlPanel()
        if (!isVideoPlaying)
            handlePlay();
        else handlePause()
    }, [handlePause, handlePlay, isVideoPlaying, showControlPanel])

    const handleCurrentTimeChanged = useCallback((time: number) => {
        if (videoRef.current) {
            videoRef.current.currentTime = time
            videoCurrentTimeChanged(time)
        }
    }, [])

    const toggleFullscreen = useCallback((e: MouseEvent) => {
        e.stopPropagation()
        if (containerRef.current) {
            const elem = containerRef.current
            if (!isFullScreen) {
                if (elem.requestFullscreen) {
                    void elem.requestFullscreen();
                } else if (elem.webkitRequestFullscreen) { /* Safari */
                    elem.webkitRequestFullscreen();
                } else if (elem.msRequestFullscreen) { /* IE11 */
                    elem.msRequestFullscreen();
                }
                setFullScreen(true)
            } else {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.webkitExitFullscreen) {
                    void document.webkitExitFullscreen();
                } else if (document.msExitFullscreen) {
                    void document.msExitFullscreen()
                }
                setFullScreen(false)
            }
        }
    }, [isFullScreen])

    const handleEpisodeSelectorClick = useCallback((e: MouseEvent) => {
        e.stopPropagation()
        setIsEpisodeSelectorOpened(true)

    }, [])


    return <div
        className="h-full w-full bg-black relative overflow-hidden"
        onClick={togglePlay}
        onMouseMove={showControlPanel}
        ref={containerRef}
        onDoubleClick={toggleFullscreen}
    >
        <video
            className="h-full w-full absolute left-0 top-0"
            ref={videoRef}
        />
        <div className={classNames("absolute bottom-0 left-0 px-[15px] h-[10%] w-full transition", {
            ["opacity-0 pointer-events-auto"]: isControlPanelVisible,
            ["opacity-100 pointer-events-none"]: isControlPanelVisible,
        })}>
            <ProgressBar duration={duration} currentTime={currentTime} onRewind={handleCurrentTimeChanged}/>
            <div className="mt-3 flex justify-between">
                <div>
                    <IconButton onClick={togglePlay}>
                        {isVideoPlaying ? <PauseIcon sx={{fontSize: "50px"}}/> :
                            <PlayArrowIcon sx={{fontSize: "50px"}}/>}
                    </IconButton>
                </div>
                <div>
                    {additionalEpisodes && <>
                        <EpisodeSelector
                            elem={episodeSelectorButtonRef.current}
                            open={isEpisodeSelectorOpened}
                            onClose={() => setIsEpisodeSelectorOpened(false)}
                            episodes={additionalEpisodes}
                        />
                        <IconButton
                            onClick={handleEpisodeSelectorClick}
                        >
                            <MovieIcon sx={{fontSize: "50px"}}/>
                        </IconButton>
                    </>}
                    <IconButton onClick={toggleFullscreen}>
                        {isFullScreen ? <FullscreenExitIcon sx={{fontSize: "50px"}}/> :
                            <FullscreenIcon sx={{fontSize: "50px"}}/>}
                    </IconButton>
                </div>
            </div>
        </div>
    </div>
}
