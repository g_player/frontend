import {Popover} from "@mui/material";
import {Episode} from "@shared/api";
import {useCallback, useState} from "react";
import s from './styles.module.scss'

interface EpisodeSelectorProps {
    open?: boolean;
    elem: HTMLButtonElement | null;
    episodes?: { [key: string]: Episode.EpisodeDTO[] };
    onClose?: VoidFunction
}

export const EpisodeSelector = ({open, elem, onClose, episodes}: EpisodeSelectorProps) => {

    const [selectedSeason, setSelectedSeason] = useState(episodes ? Object.keys(episodes)[0] : null)

    const handleSeasonClick = useCallback((seasonKey: string) => {
        setSelectedSeason(seasonKey)
    }, [])

    return <Popover
        anchorEl={elem}
        open={open ?? false}
        anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        onClose={onClose}
    >
        {episodes &&
            <div className={s.content}>
                {Object.keys(episodes).length > 1 && <div className={s.seasons}>
                    {Object.keys(episodes).map(seasonNumber => <button
                            disabled={selectedSeason === seasonNumber}
                            onClick={() => handleSeasonClick(seasonNumber)}
                        >
                            Сезон {seasonNumber}
                        </button>
                    )}
                </div>}
                {selectedSeason && episodes[selectedSeason].map(episode => <div>{episode.name}</div>)}
            </div>
        }
    </Popover>
}