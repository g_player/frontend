import classNames from "classnames";
import {MouseEvent, useCallback, useEffect, useRef, useState} from "react";

interface ProgressBarProps {
    duration: number
    currentTime: number
    onRewind: (newCurrentTime: number) => void
}

const getCurrentTimePx = (currentTime: number, duration: number, barWidth: number) => (currentTime / duration) * barWidth

export const ProgressBar = ({duration, currentTime, onRewind}: ProgressBarProps) => {
    const progressBarRef = useRef<HTMLDivElement>(null)
    const dotTranslateRef = useRef(0)

    const [timeInPx, setTimeInPx] = useState(0)
    const [progressBarHovered, setProgressBarHovered] = useState(false)

    useEffect(() => {
        if (progressBarRef.current) {
            const barWidth = progressBarRef.current.getBoundingClientRect().width
            setTimeInPx(getCurrentTimePx(currentTime, duration, barWidth))
        }
    }, [duration, currentTime]);

    const handleProgressBarClick = useCallback((e: MouseEvent) => {
        e.stopPropagation()
        if (progressBarRef.current) {
            const clickPos = e.clientX - progressBarRef.current.getBoundingClientRect().x
            const percent = (clickPos / progressBarRef.current.getBoundingClientRect().width) * 100
            const newCurrentTime = (duration / 100) * percent
            onRewind(newCurrentTime)
        }
    }, [duration, onRewind])


    return <div
        ref={progressBarRef}
        className="h-[20px] relative"
        onMouseOver={() => setProgressBarHovered(true)}
        onMouseOut={() => setProgressBarHovered(false)}
        onClick={handleProgressBarClick}
    >
        <div
            className={classNames("absolute rounded bottom-[8px] left-0 w-full bg-g-500/60 transition", {
                "h-[5px]": !progressBarHovered,
                "h-[6px]": progressBarHovered,
            })}>
            <div className="h-full absolute left-0 top-0 rounded bg-pc-400"
                 style={{
                     width: `${timeInPx}px`,
                     transform: `translateX(${dotTranslateRef.current}px)`
                 }}>
                <div
                    className={classNames("absolute right-[-8px] top-[-5px] h-[16px] w-[16px] rounded-full transition bg-pc-400", {
                        ["scale-0"]: !progressBarHovered,
                        ["scale-1"]: progressBarHovered,
                    })}
                />
            </div>
        </div>
    </div>
}