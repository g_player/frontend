import {LoginRoute} from "@pages/login";
import {MainDashboardRoute} from "@pages/mainDashboard";
import {PlayerRoute} from "@pages/player";
import {RegistrationRoute} from "@pages/registartion";
import {SettingsDashboardRoute} from "@pages/settingsDashboard";
import {createRoutesView} from "atomic-router-react";

export const Pages = createRoutesView({
    routes: [
        LoginRoute,
        RegistrationRoute,
        MainDashboardRoute,
        PlayerRoute,
        SettingsDashboardRoute
    ],
});