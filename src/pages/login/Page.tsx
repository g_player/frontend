import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";

import {routes} from "@shared/routing";
import {Button, IconButton, TextField} from "@shared/ui";
import {Link} from "atomic-router-react";
import {useUnit} from "effector-react";
import {FormEvent, useEffect, useState} from "react";
import {
    $password,
    $passwordError,
    $username,
    $usernameError,
    formSubmitted,
    pageMounted,
    passwordChanged,
    usernameChanged,
} from "./model.ts";

export const LoginPage = () => {
    const [
        username,
        usernameError,
        password,
        passwordError,
    ] = useUnit([
        $username,
        $usernameError,
        $password,
        $passwordError,
    ]);

    const [passwordVisible, setPasswordVisible] = useState(false)

    useEffect(() => {
        pageMounted();
    }, []);

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        formSubmitted();
    };

    const getUsernameErrorText = () => {
        switch (usernameError) {
            case "empty":
                return "Заполните поле";
            case "lessMin":
                return "Минимальная длинна имени пользователя - 3 символа";
            default:
                return "";
        }
    };

    const getPasswordErrorText = () => {
        switch (passwordError) {
            case "empty":
                return "Заполните поле";
            case "lessMin":
                return "Минимальная длинна пароля - 7 символов";
            default:
                return "";
        }
    };

    return <div className="w-full h-full flex justify-center items-center">
        <form onSubmit={handleSubmit} className="bg-white p-6 rounded shadow flex flex-col gap-6 w-80">
            <h1 className="antialiased text-center text-xl">Вход</h1>
            <TextField
                fullWidth
                value={username} onChange={(e) => usernameChanged(e.target.value)}
                placeholder="Логин"
                hasError={usernameError !== null}
                helperText={getUsernameErrorText()}
            />
            <TextField
                fullWidth
                type={passwordVisible ? "text" : "password"}
                value={password}
                placeholder={"Пароль"}
                onChange={(e) => passwordChanged(e.target.value)}
                endAdornment={<IconButton onClick={() => setPasswordVisible(isVisible => !isVisible)}>
                    {passwordVisible ? <VisibilityOffIcon/> : <VisibilityIcon/>}
                </IconButton>}
                hasError={passwordError !== null}
                helperText={getPasswordErrorText()}
            />
            <div className="flex flex-col gap-1">
                <Button fullWidth variant={"contained"} type="submit">Войти</Button>
                <Link to={routes.auth.registration} style={{width: "100%"}}>
                    <Button fullWidth>Регистрация</Button>
                </Link>
            </div>
        </form>
    </div>
};