import { anonymousRoute, currentRoute } from "@pages/login/model.ts";
import { LoginPage } from "@pages/login/Page.tsx";
import { PageLoader } from "@shared/ui";
import { createRouteView } from "atomic-router-react";

export const LoginRoute = {
	view: createRouteView({ route: anonymousRoute, view: LoginPage, otherwise: PageLoader }),
	route: currentRoute,
};