import { chainAnonymous, Tokens, tokensChanged } from "@app/session.ts";
import { api } from "@shared/api";
import { routes } from "@shared/routing";
import { TextFieldError, validateTextField } from "@shared/tools";
import { redirect } from "atomic-router";
import { attach, createEvent, createStore, sample } from "effector";
import { and, every, not, reset } from "patronum";

export const currentRoute = routes.auth.login;
export const anonymousRoute = chainAnonymous(currentRoute);

const loginFx = attach({ effect: api.auth.loginFx });

export const pageMounted = createEvent();
export const passwordChanged = createEvent<string>();
export const usernameChanged = createEvent<string>();
export const formSubmitted = createEvent();

export const $username = createStore("");
export const $usernameError = createStore<TextFieldError>(null);
export const $password = createStore("");
export const $passwordError = createStore<TextFieldError>(null);
export const $loginPending = loginFx.pending;
const $formValid = every({
	stores: [$usernameError, $passwordError],
	predicate: null,
});

reset({
	clock: pageMounted,
	target: [$username, $password, $usernameError, $passwordError],
});

reset({
	clock: [passwordChanged, usernameChanged],
	target: [$passwordError, $usernameError],
});


$username.on(usernameChanged, (_, login) => login);
$password.on(passwordChanged, (_, password) => password);


sample({
	clock: formSubmitted,
	source: $username,
	fn: (login) => validateTextField(login, { required: true, minLen: 3 }),
	target: $usernameError,
});


sample({
	clock: formSubmitted,
	source: $password,
	fn: (password) => validateTextField(password, { required: true, minLen: 7 }),
	target: $passwordError,
});

sample({
	clock: formSubmitted,
	source: { username: $username, password: $password },
	filter: and(not($loginPending), $formValid),
	target: loginFx,
});

sample({
	source: loginFx.doneData,
	fn: (res): Tokens => ({ refresh_token: res.refresh_token, access_token: res.access_token }),
	target: tokensChanged,
});

redirect({
	clock: loginFx.done,
	route: routes.main.dashboard,
});