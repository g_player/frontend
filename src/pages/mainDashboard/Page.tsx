import {Box, Typography} from "@mui/material";
import {$titles, $watch, Gate} from "@pages/mainDashboard/model.ts";
import {BACKEND_URL} from "@shared/api";
import {PreviewThumbnail, TitlePoster} from "@shared/ui";
import {useGate, useUnit} from "effector-react";

const baseUrl = `${BACKEND_URL}/api/v1/`

export const MainDashboardPage = () => {
    useGate(Gate)

    const [titles, watch] = useUnit([
        $titles,
        $watch
    ])

    return <Box>
        <Box>
            {titles?.map(title => <TitlePoster key={title.title_name} url={title.poser_url!} href={title.href}/>)}
        </Box>
        <Box mt={"20px"}>
            <Typography variant={"h5"}>Продолжить просмотр</Typography>
        </Box>
        <Box display={"flex"} alignItems={"flex-start"} gap={"15px"} mt={"20px"}>
            {watch?.map(w => <PreviewThumbnail
                name={w.title_name}
                episodeNumber={w.episode_number}
                seasonNumber={w.season_number}
                durationSeconds={w.duration_seconds}
                watchSeconds={w.watch_seconds}
                size={"small"}
                src={`${baseUrl}preview?filePath=${w.preview_path}`}
                href={w.href}
            />)}
        </Box>
    </Box>
};