import { authorizedRoute, currentRoute } from "@pages/mainDashboard/model.ts";
import { MainDashboardPage } from "@pages/mainDashboard/Page.tsx";
import { PageLoader } from "@shared/ui";
import { createRouteView } from "atomic-router-react";
import { MainLayout } from "@widgets/mainLayout";

export const MainDashboardRoute = {
	view: createRouteView({ route: authorizedRoute, view: MainDashboardPage, otherwise: PageLoader }),
	route: currentRoute,
	layout: MainLayout,
};