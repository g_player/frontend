import {chainAuthorized} from "@app/session.ts";
import {api, MainDashboard} from "@shared/api";
import {routes} from "@shared/routing";
import {chainRoute} from "atomic-router";
import {attach, createStore, restore, sample} from "effector";
import {createGate} from "effector-react";
import {reset} from "patronum";

type Title = Omit<MainDashboard.AvailableTitleDTO, "title_id" | "episode_id"> & { href: string }

const getHref = (titleInfo: MainDashboard.AvailableTitleDTO): string => {
    const basePath = "/watch"
    if (titleInfo.episode_id) {
        return basePath + `?episodeId=${titleInfo.episode_id}`
    }
    if (titleInfo.first_episode_id) {
        return basePath + `?episodeId=${titleInfo.first_episode_id}`
    }
    return basePath + `?titleId=${titleInfo.title_id}`
}

export const Gate = createGate()

const getAvailableTitlesFx = attach({effect: api.mainDashboard.getAvailableTitlesFx})

export const currentRoute = routes.main.dashboard;

const getAvailableTitlesRoute = chainRoute({
    route: currentRoute,
    beforeOpen: {
        effect: getAvailableTitlesFx,
        mapParams: (params) => params
    }
})
getAvailableTitlesFx.fail.watch(() => console.log("cringe"))
getAvailableTitlesFx.doneData.watch(data => console.log(data))
export const authorizedRoute = chainAuthorized(getAvailableTitlesRoute);

const $availableTitles = restore(getAvailableTitlesFx, null)
const $rawTitles = $availableTitles.map(available => available?.available_titles ?? null)
const $rawWatch = $availableTitles.map(available => available?.available_watch ?? null)

export const $titles = createStore<Title[] | null>(null)
export const $watch = createStore<Title[] | null>(null)

reset({
    clock: Gate.close,
    target: [$availableTitles, $titles, $watch]
})

sample({
    source: $rawTitles,
    filter: titles => !!titles,
    fn: (titles): Title[] => titles!.map(title => ({...title, href: getHref(title)})),
    target: $titles
})

sample({
    source: $rawWatch,
    filter: watch => !!watch,
    fn: (watch): Title[] => watch!.map(w => ({...w, href: getHref(w)})),
    target: $watch
})
