import {Video} from "@features/watchVideo";
import {$streamData} from "@pages/player/model.ts";
import {useUnit} from "effector-react";

export const PlayerPage = () => {
    const [streamData] = useUnit([$streamData])


    return <div style={{height: "100%", width: "100%"}}>
        <Video
            srcDir={streamData?.stream_dir}
            watchedTime={streamData?.watched_time}
            titleType={streamData?.title_type}
            additionalEpisodes={streamData?.additional_episodes}
        />
    </div>
}
