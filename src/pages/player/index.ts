import {authorizedRoute, currentRoute} from "@pages/player/model.ts";
import {PlayerPage} from "@pages/player/PlayerPage.tsx";
import {PageLoader} from "@shared/ui";
import {createRouteView} from "atomic-router-react";

export const PlayerRoute = {
    view: createRouteView({route: authorizedRoute, view: PlayerPage, otherwise: PageLoader}),
    route: currentRoute,
}