import {chainAuthorized} from "@app/session.ts";
import {$videoCurrentTimeSeconds, nextEpisodeStarted, videoPaused, videoPlaying} from "@features/watchVideo";
import {api, Stream} from "@shared/api";
import {routerControls, routes} from "@shared/routing";
import {querySync} from "atomic-router";
import {attach, createStore, restore, sample} from "effector";
import {createGate} from "effector-react";
import {and, interval, reset} from "patronum";

export const Gate = createGate()

const getStreamInfoFx = attach({effect: api.stream.getStreamInfoFx})
const watchEpisodeStreamFx = attach({effect: api.stream.watchEpisodeStreamFx})
const watchMovieStreamFx = attach({effect: api.stream.watchMovieStreamFx})

export const currentRoute = routes.main.player
export const authorizedRoute = chainAuthorized(currentRoute)

export const $streamData = restore(getStreamInfoFx, null)

const $episodeId = createStore<string | null>(null)
const $isVideoPlaying = createStore(false)
const $titleId = createStore<string | null>(null)
const $isMovie = $streamData.map(data => data?.title_type === "movie")
const $isSerial = $streamData.map(data => data?.title_type === "serial")
const $nextEpisodeId = $streamData.map(data => data?.next_episode_id ?? null)

querySync({
    source: {titleId: $titleId, episodeId: $episodeId},
    route: currentRoute,
    controls: routerControls
})


reset({
    clock: Gate.close,
    target: [$streamData, $isVideoPlaying, $episodeId, $titleId]
})


const {tick} = interval({start: videoPlaying, stop: videoPaused, timeout: 5000})

sample({
    source: {titleId: $titleId, episodeId: $episodeId},
    fn: ({titleId, episodeId}): Stream.GetStreamRequestParams => ({
        title_id: titleId === null ? undefined : Number(titleId),
        episode_id: episodeId === null ? undefined : Number(episodeId)
    }),
    target: getStreamInfoFx,
})

sample({
    clock: tick,
    source: ({titleId: $titleId, currentTime: $videoCurrentTimeSeconds}),
    filter: and($titleId, $videoCurrentTimeSeconds, $isMovie),
    fn: ({titleId, currentTime}): Stream.WatchMovieStreamRequestDTO => ({
        title_id: Number(titleId),
        time_seconds: currentTime
    }),
    target: watchMovieStreamFx
})

sample({
    clock: tick,
    source: ({episodeId: $episodeId, currentTime: $videoCurrentTimeSeconds}),
    filter: and($episodeId, $videoCurrentTimeSeconds, $isSerial),
    fn: ({episodeId, currentTime}): Stream.WatchEpisodeStreamRequestDTO => ({
        episode_id: Number(episodeId),
        time_seconds: currentTime
    }),
    target: watchEpisodeStreamFx
})

sample({
    clock: nextEpisodeStarted,
    source: $nextEpisodeId,
    filter: (episodeId) => !!episodeId,
    fn: (episodeId) => episodeId!.toString(),
    target: $episodeId
})

//sample({
// clock: videoCurrentTimeChanged,
//source: $streamData,
// filter: (streamData, videoCurrentTime) =>
//})
