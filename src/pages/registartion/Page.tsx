import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import {
    $password,
    $passwordError,
    $passwordVisible,
    $registrationToken,
    $registrationTokenError,
    $repeatPassword,
    $repeatPasswordError,
    $username,
    $usernameError,
    formSubmitted,
    pageMounted,
    passwordChanged,
    passwordVisibilityChanged,
    registrationTokenChanged,
    repeatPasswordChanged,
    usernameChanged,
} from "@pages/registartion/model.ts";
import {routes} from "@shared/routing";
import {Button, IconButton, TextField} from "@shared/ui";
import {Link} from "atomic-router-react";
import {useUnit} from "effector-react";
import {FormEvent, useCallback, useEffect, useState} from "react";


export const RegistrationPage = () => {

    const [
        username,
        usernameError,
        password,
        passwordVisible,
        passwordError,
        repeatPassword,
        repeatPasswordError,
        registrationToken,
        registrationTokenError,
    ] = useUnit([
        $username,
        $usernameError,
        $password,
        $passwordVisible,
        $passwordError,
        $repeatPassword,
        $repeatPasswordError,
        $registrationToken,
        $registrationTokenError,
    ]);

    const [repeatPasswordVisible, setRepeatPasswordVisible] = useState(false)

    useEffect(() => {
        pageMounted();
    }, []);

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        formSubmitted();
    };

    const getUsernameErrorText = useCallback(() => {
        switch (usernameError) {
            case "empty":
                return "Заполните поле";
            case "lessMin":
                return "Имя пользователя должно быть длиннее 3 символов";
            default:
                return "";
        }
    }, [usernameError]);

    const getPasswordErrorText = useCallback(() => {
        switch (passwordError) {
            case "empty":
                return "Заполните поле";
            case "lessMin":
                return "Пароль должен быть длиннее 7 символов";
            default:
                return "";
        }
    }, [passwordError]);

    const getRepeatPasswordErrorText = useCallback(() => {
        switch (repeatPasswordError) {
            case "empty":
                return "Заполните поле";
            case "comparedDontMatch":
                return "Пароли не совпадают";
            default:
                return "";
        }
    }, [repeatPasswordError]);

    const getRegistrationTokenErrorText = useCallback(() => {
        switch (registrationTokenError) {
            case "empty":
                return "Заполните поле";
            case "wrongToken":
                return "Неверный токен";
            default:
                return "";
        }
    }, [registrationTokenError]);

    return <div className="w-full h-full flex justify-center items-center">
        <form onSubmit={handleSubmit} className="bg-white p-6 rounded shadow flex flex-col gap-6 w-80">
            <h1 className="antialiased text-center text-xl">Регистрация</h1>
            <TextField
                fullWidth
                placeholder={"Имя пользователя"}
                value={username}
                onChange={(e) => usernameChanged(e.target.value)}
                hasError={usernameError !== null}
                helperText={getUsernameErrorText()}
            />
            <TextField
                fullWidth
                placeholder={"Пароль"}
                value={password}
                type={passwordVisible ? "text" : "password"}
                onChange={(e) => passwordChanged(e.target.value)}
                hasError={passwordError !== null}
                helperText={getPasswordErrorText()}
                endAdornment={
                    <IconButton
                        onClick={() => passwordVisibilityChanged(!passwordVisible)}>
                        {passwordVisible ?
                            <VisibilityOffIcon/> :
                            <VisibilityIcon/>}
                    </IconButton>
                }
            />
            {!passwordVisible && <TextField
                fullWidth
                placeholder={"Повторите пароль"}
                value={repeatPassword}
                onChange={(e) => repeatPasswordChanged(e.target.value)}
                hasError={repeatPasswordError !== null}
                helperText={getRepeatPasswordErrorText()}
                type={repeatPasswordVisible ? "text" : "password"}
                endAdornment={
                    <IconButton
                        onClick={() => setRepeatPasswordVisible((visible) => !visible)}>
                        {repeatPasswordVisible ?
                            <VisibilityOffIcon/> :
                            <VisibilityIcon/>}
                    </IconButton>
                }
            />}
            <TextField
                fullWidth
                placeholder={"Токен для регистрации"}
                value={registrationToken}
                onChange={(e) => registrationTokenChanged(e.target.value)}
                hasError={registrationTokenError !== null}
                helperText={getRegistrationTokenErrorText()}
            />

            <div className="flex flex-col gap-1">
                <Button fullWidth variant={"contained"} type="submit">Зарегистрироваться</Button>
                <Link to={routes.auth.login} style={{width: "100%"}}>
                    <Button fullWidth>Авторизация</Button>
                </Link>
            </div>
        </form>
    </div>;

};