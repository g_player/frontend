import { anonymousRoute, currentRoute } from "@pages/registartion/model.ts";
import { RegistrationPage } from "@pages/registartion/Page.tsx";
import { PageLoader } from "@shared/ui";
import { createRouteView } from "atomic-router-react";

export const RegistrationRoute = {
	view: createRouteView({ route: anonymousRoute, view: RegistrationPage, otherwise: PageLoader }),
	route: currentRoute,
};