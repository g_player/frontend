import { chainAnonymous } from "@app/session.ts";
import { SuccessSnackbarMessageKey, successSnackbarOpened } from "@entities/snackbar";
import { api, Auth } from "@shared/api";
import { routes } from "@shared/routing";
import { TextFieldError, validateTextField } from "@shared/tools";
import { redirect } from "atomic-router";
import { attach, createEvent, createStore, sample } from "effector";
import { and, every, not, reset } from "patronum";

type TokenFieldErrors = TextFieldError | "wrongToken"

const registrationFx = attach({ effect: api.auth.registrationFx });

export const currentRoute = routes.auth.registration;
export const anonymousRoute = chainAnonymous(currentRoute);

export const pageMounted = createEvent();
export const usernameChanged = createEvent<string>();
export const passwordChanged = createEvent<string>();
export const passwordVisibilityChanged = createEvent<boolean>();
export const repeatPasswordChanged = createEvent<string>();
export const registrationTokenChanged = createEvent<string>();
export const formSubmitted = createEvent();

export const $username = createStore("");
export const $usernameError = createStore<TextFieldError>(null);
export const $password = createStore("");
export const $passwordVisible = createStore(false);
export const $passwordError = createStore<TextFieldError>(null);
export const $repeatPassword = createStore("");
export const $repeatPasswordError = createStore<TextFieldError>(null);
export const $registrationToken = createStore("");
export const $registrationTokenError = createStore<TokenFieldErrors>(null);
const $formValid = every({
	stores: [$usernameError, $passwordError, $repeatPasswordError, $registrationTokenError],
	predicate: null,
});
const $registrationRequestPending = registrationFx.pending;

reset({
	clock: pageMounted,
	target: [
		$username,
		$usernameError,
		$password,
		$passwordVisible,
		$passwordError,
		$repeatPassword,
		$repeatPasswordError,
		$registrationToken,
		$registrationTokenError,
	],
});

reset({
	clock: [usernameChanged, passwordChanged, repeatPasswordChanged, registrationTokenChanged],
	target: [$usernameError, $passwordError, $repeatPasswordError, $registrationTokenError],
});

$repeatPassword.reset(passwordVisibilityChanged);

$username.on(usernameChanged, (_, username) => username);
$password.on(passwordChanged, (_, password) => password);
$passwordVisible.on(passwordVisibilityChanged, (_, isVisible) => isVisible);
$repeatPassword.on(repeatPasswordChanged, (_, password) => password);
$registrationToken.on(registrationTokenChanged, (_, token) => token);

sample({
	clock: formSubmitted,
	source: $username,
	fn: (username) => validateTextField(username, { required: true, minLen: 3 }),
	target: $usernameError,
});

sample({
	clock: formSubmitted,
	source: $password,
	fn: (password) => validateTextField(password, { required: true, minLen: 7 }),
	target: $passwordError,
});

sample({
	clock: formSubmitted,
	source: { password: $password, passwordVisible: $passwordVisible, repeatPassword: $repeatPassword },
	fn: ({ password, passwordVisible, repeatPassword }) => {
		if (passwordVisible) {
			return null;
		}
		return validateTextField(repeatPassword, {
			required: true,
			compareOtherField: { value: password },
		});
	},
	target: $repeatPasswordError,
});

sample({
	clock: formSubmitted,
	source: $registrationToken,
	fn: (token) => validateTextField(token, { required: true }),
	target: $registrationTokenError,
});

sample({
	clock: formSubmitted,
	source: { username: $username, password: $password, registrationToken: $registrationToken },
	filter: and(not($registrationRequestPending), $formValid),
	fn: ({ username, password, registrationToken }): Auth.RegistrationRequestDTO => ({
		password,
		username,
		registration_token: registrationToken,
	}),
	target: registrationFx,
});

sample({
	source: registrationFx.failData,
	filter: (err) => err.status === 404,
	fn: (): TokenFieldErrors => "wrongToken",
	target: $registrationTokenError,
});

sample({
	clock: registrationFx.done,
	fn: (): SuccessSnackbarMessageKey => "registrationComplete",
	target: successSnackbarOpened,
});

redirect({
	clock: registrationFx.done,
	route: routes.auth.login,
});