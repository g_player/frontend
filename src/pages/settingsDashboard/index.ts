import {PageLoader} from "@shared/ui";
import {MainLayout} from "@widgets/mainLayout";
import {createRouteView} from "atomic-router-react";
import {adminRoute, currentRoute} from './model'
import {SettingsDashboardPage} from './ui/Page.tsx'

export const SettingsDashboardRoute = {
    view: createRouteView({route: adminRoute, view: SettingsDashboardPage, otherwise: PageLoader}),
    route: currentRoute,
    layout: MainLayout
}