import {chainAdmin} from "@app/session.ts";
import {routes} from "@shared/routing";

export const currentRoute = routes.settings.dashboard
export const adminRoute = chainAdmin(currentRoute)