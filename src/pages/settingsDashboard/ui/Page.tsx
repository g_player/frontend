import {DownloadPanel} from "@widgets/download";
import {TitlesPanel} from "@widgets/titlesPanel";
import {UserPanel} from "@widgets/users";

export const SettingsDashboardPage = () => {
    return <div className="p-3 flex w-full h-full gap-3 flex-wrap">
        <div>
            <UserPanel/>
        </div>
        <div>
            <DownloadPanel/>
        </div>
        <div>
            <TitlesPanel/>
        </div>
    </div>;
}