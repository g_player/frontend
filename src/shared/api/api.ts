import {logout} from "@app/session.ts";
import {showMessage} from "@shared/floatingAlert";
import axios, {AxiosResponse} from "axios";
import {createEffect} from "effector";
import {
    Auth,
    DownloadsSettings,
    Episode,
    ErrorResponse,
    ExternalTitle,
    MainDashboard,
    Os,
    Stream,
    Title,
    UserSettings
} from "./types.ts";

export const BACKEND_URL = import.meta.env.MODE === "development" ? `${import.meta.env.VITE_BACKEND_SCHEMA}${import.meta.env.VITE_BACKEND_HOST}${import.meta.env.VITE_BACKEND_PORT}` : ""
export const BACKEND_WS_URL = import.meta.env.MODE === "development" ? `${import.meta.env.VITE_BACKEND_WS_SCHEMA}${import.meta.env.VITE_BACKEND_HOST}${import.meta.env.VITE_BACKEND_PORT}` : ""

export const httpClient = axios.create({
    baseURL: BACKEND_URL,
});

interface Request {
    path: string;
    method: "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
    params?: unknown;
    body?: unknown;
    formData?: boolean;
}

export const requestFx = createEffect<Request, any, ErrorResponse>(async request => {
    if (request.formData) {
        const formData = new FormData();
        if (request.body) {
            const body = request.body as { [key: string]: any };
            Object.keys(body).forEach(key => {
                formData.append(key, body[key]);
            });
            request.body = formData;
        }
    }
    let headers: { [key: string]: string | undefined } = {
        "Content-Type": "application/json",
        "Authorization": undefined,
    };
    const tokens = localStorage.getItem("tokens");
    if (request.formData) {
        headers = {...headers, "Content-Type": "multipart/form-data"};
    }
    if (tokens) {
        const {access_token} = JSON.parse(tokens);
        headers = {...headers, "Authorization": `Bearer ${access_token}`};
    }
    try {
        const response = await httpClient({
            method: request.method,
            url: request.path,
            data: request.body,
            params: request.params,
            headers: headers,
        });
        return response.data;
    } catch (err: any) {
        const status = err.response.status;
        try {
            const tokens = JSON.parse(localStorage.getItem("tokens") ?? "") as {
                refresh_token: string,
                access_token: string
            };
            const refreshToken = tokens.refresh_token;
            if (status === 401 && refreshToken) {
                try {
                    const newTokens = await api.auth.refreshTokenFx({refresh_token: refreshToken});
                    err.config.headers["Authorization"] = `Bearer ${newTokens.access_token}`;
                    const retryResponse = await httpClient.request(err.config);
                    return retryResponse.data;
                } catch {
                    logout();
                    return Promise.reject(err);
                }
            } else if (status === 401 && !refreshToken) {
                logout();
            } else {
                showMessage({message: err.response.data.message, closeTimeoutMs: 3000, type: "error"})
            }
        } catch (e) {
            logout()
        }
        return Promise.reject(err);
    }
});

export const api = {
    auth: {
        loginFx: createEffect<Auth.LoginRequestDTO, Auth.Tokens, ErrorResponse>((body) => requestFx({
                method: "POST",
                path: "/api/v1/auth/login",
                body,
            }),
        ),
        refreshTokenFx: createEffect<Auth.RefreshTokenRequestDTO, Auth.RefreshTokenResponseDTO, ErrorResponse>(async (body) => {
            const tokens = await httpClient.post<Auth.RefreshTokenRequestDTO, AxiosResponse<Auth.RefreshTokenResponseDTO>>("/api/v1/auth/refresh", body)
            const {access_token, refresh_token} = tokens.data
            localStorage.setItem("tokens", JSON.stringify({access_token, refresh_token}));
            return {access_token, refresh_token};
        }),
        registrationFx: createEffect<Auth.RegistrationRequestDTO, void, ErrorResponse>((body) => requestFx({
            method: "POST",
            path: "/api/v1/auth/registration",
            body,
        })),
        refreshToken: (data: Auth.RefreshTokenRequestDTO) => httpClient.post<Auth.RefreshTokenRequestDTO, AxiosResponse<Auth.RefreshTokenResponseDTO>>("/api/v1/auth/refresh", data),
    },
    mainDashboard: {
        getAvailableTitlesFx: createEffect<void, MainDashboard.GetAvailableTitlesDTO>(() => requestFx({
            method: "GET",
            path: "/api/v1/titles/available"
        }))
    },
    usersSettings: {
        getUsersFx: createEffect<{
            offset?: number,
            count?: number
        }, UserSettings.GetUsersResponseDTO, ErrorResponse>((params) => requestFx({
            method: "GET",
            path: "/api/v1/users",
            params: params,
        })),
        createRegistrationTokenFx: createEffect<void, UserSettings.CreateRegistrationTokenResponseDTO, ErrorResponse>(() => requestFx({
            method: "POST",
            path: "/api/v1/auth/registrationToken",
        })),
        getUsersRegistrationTokensFx: createEffect<void, UserSettings.GetUsersRegistrationTokensResponseDTO, ErrorResponse>(() => requestFx({
            method: "GET",
            path: "/api/v1/auth/registrationTokens",
        })),
        deleteUserRegistrationTokenFx: createEffect<number, void, ErrorResponse>((tokenId) => requestFx({
            method: "DELETE",
            path: `/api/v1/auth/registrationToken/${tokenId}`,
        }))
    },
    downloadsSettings: {
        createTorrentByFileFx: createEffect<DownloadsSettings.CreateTorrentByFileDTO, void, ErrorResponse>((body) => requestFx({
            method: "POST",
            path: "/api/v1/torrent/file",
            formData: true,
            body,
        })),
        getDownloadsFx: createEffect<void, DownloadsSettings.GetDownloadsResponseDTO, ErrorResponse>(() => requestFx({
            method: "GET",
            path: "/api/v1/torrents",
        })),
        controlTorrentFx: createEffect<DownloadsSettings.ControlTorrentRequestDTO, void, ErrorResponse>((body) => requestFx({
            method: "POST",
            path: "/api/v1/torrent/control",
            body,
        })),
        convertTorrentToHlsFx: createEffect<DownloadsSettings.ConvertTorrentToHlsRequestDTO, void>((body) => requestFx({
            method: "POST",
            path: "/api/v1/torrents/convertTorrentToHls",
            body
        }))
    },
    title: {
        getTitlesFx: createEffect<Title.GetTitlesRequestParams, Title.GetTitlesResponseDTO, ErrorResponse>(({pagination}) => requestFx({
            method: "GET",
            path: "/api/v1/titles",
            params: pagination,
        })),
        findExternalTitlesFx: createEffect<ExternalTitle.FindExternalTitlesRequestParams, ExternalTitle.FindExternalTitlesResponseDTO, ErrorResponse>((params) => requestFx({
            method: "GET",
            path: "/api/v1/title/external",
            params: params,
        })),
        createTitleFx: createEffect<Title.CreateTitleDTO, void, ErrorResponse>((body) => requestFx({
            method: "POST",
            path: "/api/v1/title",
            body,
        })),
        getTileFx: createEffect<{
            titleId: number
        }, Title.GetTitleResponseDTO, ErrorResponse>(({titleId}) => requestFx({
            method: "GET",
            path: `/api/v1/title/${titleId}`,
        })),
        controlTitleFx: createEffect<Title.ControlTitleRequestDTO, void, ErrorResponse>((body) => requestFx({
            method: "POST",
            path: "/api/v1/title/control",
            body
        })),
        updateMoviePathFx: createEffect<Title.UpdateMovieFileRequestDTO, void>(body => requestFx({
            method: "POST",
            path: "/api/v1/title/updateMovieFile",
            body
        }))
    },
    episode: {
        getEpisodesFx: createEffect<Episode.GetEpisodesRequestParams, Episode.GetEpisodesResponseDTO>((params) => requestFx({
            method: "GET",
            path: "/api/v1/episodes",
            params,
        })),
        updateFilePathFx: createEffect<Episode.UpdateEpisodeFilePathRequestDTO, void>((body) => requestFx({
            method: "PATCH",
            path: "/api/v1/episode/updateFilePath",
            body,
        })),
        updateEpisodesPathsForSeasonFx: createEffect<Episode.UpdateEpisodesPathsForSeasonRequestDTO, void>((body) => requestFx({
            method: "PATCH",
            path: "/api/v1/episode/updateEpisodesPathsForSeason",
            body
        }))
    },

    os: {
        getTitlesDirFx: createEffect<Os.GetFilesRequestParams | void, Os.GetFilesResponseDTO>((params) => requestFx({
            method: "GET",
            path: `/api/v1/os/dir/${params!.dir}`,
            params: {path: params!.path},
        })),
    },

    stream: {
        getStreamInfoFx: createEffect<Stream.GetStreamRequestParams, Stream.GetStreamResponseDTO>((params) => requestFx({
            method: "GET",
            path: '/api/v1/stream/info',
            params
        })),
        watchEpisodeStreamFx: createEffect<Stream.WatchEpisodeStreamRequestDTO, void>((body) => requestFx({
            method: "POST",
            path: "/api/v1/stream/watch/episode",
            body
        })),
        watchMovieStreamFx: createEffect<Stream.WatchMovieStreamRequestDTO, void>((body) => requestFx({
            method: "POST",
            path: "/api/v1/stream/watch/movie",
            body
        }))
    }
};