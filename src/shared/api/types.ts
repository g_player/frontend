export interface Pagination {
    offset: number;
    total: number;
    count: number;
}

export interface ErrorResponse {
    error?: {
        data?: {
            message?: string
        }
    };
    status: number;
}

export namespace MainDashboard {
    export type AvailableTitleDTO = {
        title_name: string
        title_alternative_name: string
        title_id?: number
        episode_id?: number
        title_type: "movie" | "serial"
        poser_url?: string
        preview_path?: string
        episode_number?: number
        season_number?: number
        published: boolean
        episode_name?: string
        episode_alternative_name?: string
        duration_seconds: number
        watch_seconds?: number
        first_episode_id?: number
    }
    export type GetAvailableTitlesDTO = {
        available_titles: AvailableTitleDTO[];
        available_watch: AvailableTitleDTO[];
    }
}

export namespace Auth {
    export type Tokens = {
        access_token: string;
        refresh_token: string;
    }

    export type RefreshTokenRequestDTO = {
        refresh_token: string;
    }

    export type RefreshTokenResponseDTO = Tokens

    export type LoginRequestDTO = {
        username: string;
        password: string;
    }

    export type RegistrationRequestDTO = {
        username: string;
        password: string;
        registration_token: string;
    }
}

export namespace UserSettings {
    type UserTokenDTO = {
        id: number;
        token: string;
        expired_at: string;
    }

    export type UserDTO = {
        id: number,
        username: string,
        role: "user" | "admin"
    }

    export type GetUsersResponseDTO = {
        pagination: Pagination
        users: UserDTO[]
    }

    export type CreateRegistrationTokenResponseDTO = {
        registration_token: string
    }

    export type GetUsersRegistrationTokensResponseDTO = {
        occupied_tokens?: (UserTokenDTO & { username: string })[]
        free_tokens?: UserTokenDTO[]
    }
}

export namespace Title {
    export type TitleDTO = {
        id: number;
        name?: string;
        alternative_name: string;
        description?: string;
        year: number;
        rating_kp: number;
        rating_imdb: number;
        poster_preview_url: string;
        poster_url: string;
        backdrop_preview_url: string;
        backdrop_url: string;
        published: boolean;
        season_count?: number;
        type: "movie" | "serial";
    }

    export enum TitleControlAction {
        PUBLISH = "publish",
        WITHDRAW = "withdraw",
    }

    export type GetTitlesRequestParams = {
        pagination?: {
            offset: number;
            count: number;
        };
    }

    export type GetTitlesResponseDTO = {
        titles: TitleDTO[],
        pagination: Pagination
    }

    export type CreateTitleDTO = {
        external_id: number;
    }

    export type ControlTitleRequestDTO = {
        title_id: number,
        action: TitleControlAction
    }

    export type GetTitleResponseDTO = TitleDTO

    export type UpdateMovieFileRequestDTO = {
        title_id: number
        source_file_path: string
        source_resolution: string
    }
}

export namespace ExternalTitle {

    export type ExternalTitleDTO = {
        id: number;
        name?: string;
        description?: string;
        poster_url: string;
        short_description: string;
        alternative_name: string;
        type: "movie" | "serial";
        year: number;
    }

    export type FindExternalTitlesRequestParams = {
        name: string;
    }

    export type FindExternalTitlesResponseDTO = ExternalTitleDTO[]
}

export namespace DownloadsSettings {
    export type CreateTorrentByFileDTO = {
        file: File;
    }

    export type DownloadDTO = {
        id: number;
        name: string;
        speed: string;
        completion_percentage: number;
        status: "loading" | "paused";
    }

    export type GetDownloadsResponseDTO = {
        downloads: DownloadDTO[]
    }

    export enum TorrentControlAction {
        PAUSE = "pause",
        CONTINUE = "continue",
        DELETE = "delete",
        DELETE_WITH_FILES = "delete_with_files"
    }

    export type ControlTorrentRequestDTO = {
        id: number,
        action: TorrentControlAction
    }
    export type ConvertTorrentToHlsRequestDTO = {
        path: string
        is_dir?: boolean,
        source_resolution: "360p" | "720p" | "1080p" | "2k" | "4k"
    }
}

export namespace Episode {
    export type EpisodeDTO = {
        id: number
        name: string
        en_name: string
        thumbnail_url: string
        duration_minutes: string
        description?: string
        episode_number: number
        season_number: number
        file_path?: string
    }

    export type GetEpisodesRequestParams = {
        title_id: number,
        season: number
    }

    export type GetEpisodesResponseDTO = EpisodeDTO[]

    export type UpdateEpisodeFilePathRequestDTO = {
        episode_id: number
        file_path: string | null
    }

    export type UpdateEpisodesPathsForSeasonRequestDTO = {
        title_id: number
        season_number: number
        source_dir_path: string
        source_resolution: string
    }
}

export namespace Os {
    export type FileDTO = {
        is_dir: boolean,
        local_path: string,
        file_name: string
    }

    export type GetFilesRequestParams = {
        path?: string
        dir: "torrents" | "titles"
    }

    export type GetFilesResponseDTO = FileDTO[]
}

export namespace Stream {
    export type GetStreamRequestParams = {
        title_id?: number
        episode_id?: number
    }

    export type GetStreamResponseDTO = {
        stream_dir: string
        title_type: "movie" | "serial"
        watched_time?: number
        available_episode_id?: number
        next_episode_id?: number
        additional_episodes?: { [key: string]: Episode.EpisodeDTO[] }
    }

    export type WatchMovieStreamRequestDTO = {
        title_id: number
        time_seconds: number
    }

    export type WatchEpisodeStreamRequestDTO = {
        episode_id: number
        time_seconds: number
    }
}
