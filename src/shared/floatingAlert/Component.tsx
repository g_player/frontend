import {$messageToShow, hideMessage} from "@shared/floatingAlert/model.ts";
import classNames from "classnames";
import {useUnit} from "effector-react";
import {useEffect, useMemo, useRef, useState} from "react";

const ANIMATION_DURATION = 1000;

export const FloatingAlert = () => {
    const [message] = useUnit([$messageToShow])
    const [isVisible, setIsVisible] = useState<boolean>(false)
    const timeoutRef = useRef<null | NodeJS.Timeout>(null);
    const popTimeoutRef = useRef<null | NodeJS.Timeout>(null);
    const closeMs = useMemo(() => message?.closeTimeoutMs ? message.closeTimeoutMs + ANIMATION_DURATION : ANIMATION_DURATION, [message])

    useEffect(() => {
        if (message) {
            if (timeoutRef.current) {
                clearTimeout(timeoutRef.current)
            }
            setIsVisible(true)
            timeoutRef.current = setTimeout(() => {
                setIsVisible(false)
            }, closeMs)
        }
    }, [closeMs, message]);

    useEffect(() => {
        if (!isVisible) {
            if (popTimeoutRef.current) {
                clearTimeout(popTimeoutRef.current)
            }
            popTimeoutRef.current = setTimeout(() => {
                hideMessage()
            },)
        }
    }, [isVisible]);

    useEffect(() => {
        return () => {
            if (popTimeoutRef.current) {
                clearTimeout(popTimeoutRef.current)
            }
            if (timeoutRef.current) {
                clearTimeout(timeoutRef.current)
            }
        }
    }, []);


    return <div className={classNames(
        `fixed bg-red-500 text-white max-w-96 shadow rounded p-3 bottom-6 right-6 transition-all ${isVisible ? "translate-x-[0px]" : "translate-x-[4000px]"} duration-${ANIMATION_DURATION} z-50`, {}
    )}>
        {message?.message}
    </div>
}