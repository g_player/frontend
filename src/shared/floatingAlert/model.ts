import {createEvent, createStore} from "effector";

type AlertType = "error" | "success" | "warning" | "info";
type OpenAlertParams = {
    type: AlertType
    message?: string
    closeTimeoutMs?: number
}

export const showMessage = createEvent<OpenAlertParams>()
export const hideMessage = createEvent()

const $messageQueue = createStore<OpenAlertParams[]>([])
export const $messageToShow = $messageQueue.map(messages => messages[0] ?? null)

$messageQueue.on(showMessage, (currentMessages, newMessage) => [...currentMessages, newMessage])
$messageQueue.on(hideMessage, (currentMessages) => {
    const msg = [...currentMessages]
    msg.shift()
    return msg
})