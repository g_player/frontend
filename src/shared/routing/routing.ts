import {appStarted} from "@shared/config";
import {createHistoryRouter, createRoute, createRouterControls} from "atomic-router";
import {sample} from "effector";
import {createBrowserHistory} from "history";

export const routerControls = createRouterControls()

export const routes = {
    auth: {
        login: createRoute(),
        registration: createRoute(),
    },
    main: {
        dashboard: createRoute(),
        player: createRoute<{ episodeId?: number, titleId?: number }>()
    },
    settings: {
        dashboard: createRoute(),
    },
};

export const router = createHistoryRouter({
    routes: [
        {path: "/login", route: routes.auth.login},
        {path: "/registration", route: routes.auth.registration},
        {path: "/", route: routes.main.dashboard},
        {path: "/settings/dashboard", route: routes.settings.dashboard},
        {path: "/watch", route: routes.main.player}
    ],
    controls: routerControls
});

sample({
    clock: appStarted,
    fn: () => {
        return createBrowserHistory();
    },
    target: router.setHistory,
});