import { TextFieldError } from "@shared/tools";
import { createEffect } from "effector";

export const copyTextToClipboardFx = createEffect((text: string) => {
	return navigator.clipboard.writeText(text);
});

export function validateTextField(value: string, options: {
	required?: boolean,
	minLen?: number,
	maxLen?: number,
	compareOtherField?: {
		value: string
	}
}): TextFieldError {
	if (options.required && value.trim() === "") {
		return "empty";
	}
	if (options.maxLen && value.length > options.maxLen) {
		return "greaterMax";
	}
	if (options.minLen && value.length < options.minLen) {
		return "lessMin";
	}
	if (options.compareOtherField && value !== options.compareOtherField.value) {
		return "comparedDontMatch";
	}
	return null;
}