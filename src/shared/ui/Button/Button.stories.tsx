import {Meta, StoryObj} from "@storybook/react";
import {fn} from "@storybook/test";
import {Button} from "./Button.tsx";

const meta: Meta<typeof Button> = {
    title: "Base/Button",
    component: Button
}

export default meta;

type Story = StoryObj<typeof meta>

export const Contained: Story = {
    args: {
        onClick: fn(),
        children: "Вход",
        variant: "contained"
    }
}

export const Text: Story = {
    args: {
        onClick: fn(),
        children: "Вход",
        variant: "text"
    }
}

export const Outlined: Story = {
    args: {
        onClick: fn(),
        children: "Вход",
        variant: "outlined"
    }
}