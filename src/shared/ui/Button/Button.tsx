import classNames from "classnames";
import {ButtonHTMLAttributes, ReactNode} from "react";

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    children?: ReactNode;
    variant?: "contained" | "outlined" | "text";
    fullWidth?: boolean;
}

export const Button = ({children, variant = "text", fullWidth, className, ...props}: ButtonProps) => {
    return <button className={
        classNames(`transition p-3 rounded`, {
            ["bg-pc-200 hover:bg-pc-300 active:bg-pc-400 dark:bg-pc-400 dark:hover:bg-pc-500 dark:active:bg-pc-600 shadow"]: variant === "contained",
            ["border border-pc-200 text-pc-200 hover:bg-pc-200/30 active:bg-pc-400/30 dark:border-pc-400 dark:text-pc-400 dark:hover:bg-pc-400/30 dark:active:bg-pc-600/30"]: variant === "outlined",
            [" text-pc-200 hover:bg-pc-200/30 active:bg-pc-400/30 dark:text-pc-400 dark:hover:bg-pc-400/30 dark:active:bg-pc-600/30"]: variant === "text",
            ["w-full"]: fullWidth,
            [className!]: !!className
        })
    } {...props}>
        {children}
    </button>;
}