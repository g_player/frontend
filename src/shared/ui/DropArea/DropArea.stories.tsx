import {Meta, StoryObj} from "@storybook/react";
import {DropArea} from './DropArea.tsx'

const meta: Meta<typeof DropArea> = {
    title: "Base/DropArea",
    component: DropArea
}

export default meta;

type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {}
}