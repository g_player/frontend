import classNames from "classnames";
import {DragEvent, useCallback, useRef, useState} from "react";

interface DropAreaProps {
    onChange?: (val: File | File[]) => void
    multiple?: boolean;
}

export const DropArea = ({onChange, multiple}: DropAreaProps) => {
    const inputRef = useRef<HTMLInputElement>(null);
    const [isHovered, setIsHovered] = useState(false);

    const handleChange = useCallback((files: FileList | null) => {
        if (files && onChange) {
            const arr = Array.from(files)
            if (multiple) {
                onChange(arr)
            } else {
                onChange(arr[0])
            }
        }
    }, [multiple, onChange])

    const handleClick = () => {
        if (inputRef.current) {
            inputRef.current.click()
        }
    }

    const handleDrop = useCallback((e: DragEvent) => {
        e.preventDefault()
        handleChange(e.dataTransfer.files)
    }, [handleChange])

    return <div
        className={classNames(
            "border-2 border-dashed transition relative p-3 cursor-pointer", {
                ["border-pc-200/70"]: !isHovered,
                ["border-pc-400/70"]: isHovered,
            }
        )}
        onClick={handleClick}
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
        onDrop={handleDrop}
    >
        <div className="h-full w-full absolute top-0 left-0"></div>
        <h3 className={classNames("text-center transition", {
            ["text-pc-200"]: !isHovered,
            ["text-pc-400"]: isHovered,
        })}
        >
            Перетащите файл сюда
        </h3>
        <input
            type="file"
            ref={inputRef}
            onChange={(e) => handleChange(e.target.files)}
            className="hidden"
            multiple={multiple}
        />
    </div>
}