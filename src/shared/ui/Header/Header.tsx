import LogoutIcon from "@mui/icons-material/Logout";
import SettingsIcon from "@mui/icons-material/Settings";
import {IconButton} from "@shared/ui";
import {RouteInstance} from "atomic-router";
import {Link} from "atomic-router-react";

interface HeaderProps {
    showSettings?: boolean;
    settingsButtonRoute: RouteInstance<any>;
    onLogoutButtonClick?: VoidFunction;
}

export const Header = ({showSettings, settingsButtonRoute, onLogoutButtonClick}: HeaderProps) => {

    return <div className="fullWidth p-1 flex justify-between bg-white shadow h-14">
        <div></div>
        <div className="flex space-x-1 items-center">
            {showSettings && <Link to={settingsButtonRoute}>
                <IconButton variant="transparent">
                    <SettingsIcon/>
                </IconButton>
            </Link>}

            <IconButton onClick={onLogoutButtonClick}>
                <LogoutIcon/>
            </IconButton>
        </div>
    </div>;
};