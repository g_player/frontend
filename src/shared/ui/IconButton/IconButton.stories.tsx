import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import {Meta, StoryObj} from "@storybook/react";
import {fn} from "@storybook/test";
import {IconButton} from "./IconButton.tsx";

const meta: Meta<typeof IconButton> = {
    title: "Base/IconButton",
    component: IconButton
}

export default meta;

type Story = StoryObj<typeof meta>

export const Filled: Story = {
    args: {
        onClick: fn(),
        children: <PlayArrowIcon/>,
        variant: "filled"
    }
}

export const Transparent: Story = {
    args: {
        onClick: fn(),
        children: <PlayArrowIcon/>,
        variant: "transparent"
    }
}

export const Outlined: Story = {
    args: {
        onClick: fn(),
        children: <PlayArrowIcon/>,
        variant: "outlined"
    }
}