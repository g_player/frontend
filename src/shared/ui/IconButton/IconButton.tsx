import classNames from "classnames";
import {ButtonHTMLAttributes, ReactNode} from "react";

interface IconButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    variant?: "outlined" | "filled" | "transparent";
    children?: ReactNode;
}

export const IconButton = (
    {
        variant = "transparent",
        type = "button",
        children,
        className,
        ...props
    }: IconButtonProps) => {
    return <button className={
        classNames("rounded-full p-3 transition size-auto text-xs", {
            ["bg-pc-200 hover:bg-pc-300 active:bg-pc-400 dark:bg-pc-400 text-white dark:hover:bg-pc-500 dark:active:bg-pc-600"]: variant === "filled",
            ["hover:bg-pc-200/30 active:bg-pc-400/30 dark:hover:bg-pc-400/30 dark:active:bg-pc-600/30 text-pc-400"]: variant === "transparent",
            ["border border-pc-200 hover:bg-pc-200/30 active:bg-pc-400/30 dark:border-pc-400 text-pc-400 dark:hover:bg-pc-400/30 dark:active:bg-pc-600/30"]: variant === "outlined",
            [className!]: !!className
        })} type={type} {...props}>
        {children}
    </button>;
}