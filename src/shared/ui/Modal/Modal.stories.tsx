import {Button} from "@shared/ui";
import {Modal} from "@shared/ui/Modal/Modal.tsx";
import {Meta, StoryObj} from "@storybook/react";
import {ComponentProps, useState} from "react";

const Component = ({children, ...props}: Omit<ComponentProps<typeof Modal>, "onClose">) => {
    const [opened, setOpened] = useState(false);
    return <div>
        <Button onClick={() => setOpened(true)}>Open</Button>
        <Modal opened={opened} onClose={() => setOpened(false)} {...props}>
            {children}
        </Modal>
    </div>
}

const meta: Meta<typeof Modal> = {
    title: "Base/Modal",
    component: Component
}

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        children: <div>Some info</div>
    }
}

export const CloseButton: Story = {
    args: {
        children: <div>Some info</div>,
        withCloseButton: true
    }
}