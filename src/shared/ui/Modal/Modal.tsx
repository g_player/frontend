import CloseIcon from '@mui/icons-material/Close';
import {IconButton} from "@shared/ui";
import {forwardRef, ReactNode, useCallback, useEffect, useImperativeHandle, useRef} from "react";
import {createPortal} from "react-dom";

interface ModalProps {
    children?: ReactNode;
    opened?: boolean
    onClose?: VoidFunction
    withCloseButton?: boolean
}

export const Modal = forwardRef<
    { isClickTarget: (e: MouseEvent) => boolean | null },
    ModalProps
>(({
       children,
       onClose,
       opened,
       withCloseButton
   }: ModalProps, ref) => {
    const wrapperRef = useRef<HTMLDivElement>(null)
    const modalContainerRef = useRef<HTMLDivElement>(null);

    useImperativeHandle(ref, () => {
        return {
            isClickTarget(e: MouseEvent) {
                const isModalClick = modalContainerRef.current && modalContainerRef.current.contains(e.target as Element)
                const isWrapperClick = wrapperRef.current && wrapperRef.current.contains(e.target as Element)
                return isModalClick || isWrapperClick
            }
        }
    }, [])

    const handleClose = useCallback(() => {
        if (onClose) onClose();
    }, [onClose])

    const handleClick = useCallback((e: MouseEvent) => {
        if (modalContainerRef.current && opened && !modalContainerRef.current.contains(e.target as HTMLDivElement)) {
            handleClose()
        }
    }, [handleClose, opened])

    useEffect(() => {
        if (opened) {
            document.addEventListener("mousedown", handleClick);
        }
        return () => {
            document.removeEventListener("mousedown", handleClick);
        }
    }, [handleClick, opened]);

    if (!opened) return null;

    return createPortal(<div
        className="absolute z-40 h-full w-full bg-g-800/70 top-0 left-0 flex justify-center items-center"
        ref={wrapperRef}
    >
        <div ref={modalContainerRef} className="relative bg-white rounded shadow-2xl p-3">
            {withCloseButton && <IconButton
                className="absolute right-[-45px] top-[-45px]"
                onClick={handleClose}
            >
                <CloseIcon/>
            </IconButton>}
            {children}
        </div>
    </div>, document.body);
})