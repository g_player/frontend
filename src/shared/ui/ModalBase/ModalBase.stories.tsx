import { Box, Button } from "@mui/material";
import { ModalBase } from "@shared/ui/ModalBase/ModalBase.tsx";
import { Meta, StoryObj } from "@storybook/react";
import { fn } from "@storybook/test";

const meta: Meta<typeof ModalBase> = {
	title: "Atoms/ModalBase",
	component: ModalBase,
};

export default meta;

type Story = StoryObj<typeof meta>

export const Default: Story = {
	args: {
		open: true,
		onClose: fn(),
		children: <Box
			sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", maxWidth: 400 }}>
			<Box>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, architecto asperiores beatae eos ipsam laborum
				odit,
				omnis perspiciatis qui quod tempore, tenetur vel voluptatibus. Alias animi impedit magnam molestiae porro
				praesentium suscipit tenetur unde vitae? Animi consectetur deserunt dolores maiores, temporibus tenetur totam
				voluptatum. Accusantium aliquam at iste ullam voluptate?
			</Box>
			<Button>Modal inner</Button>
		</Box>,
	},
};