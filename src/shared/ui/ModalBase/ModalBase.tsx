import CloseIcon from "@mui/icons-material/Close";
import { IconButton, Modal, Paper, styled } from "@mui/material";
import { ReactNode, useCallback } from "react";

export interface ModalBaseProps {
	open?: boolean;
	onClose?: VoidFunction;
}

interface Props extends ModalBaseProps {
	children?: ReactNode;
}

const ModalBasePaper = styled(Paper)(({ theme }) => ({
	position: "absolute",
	top: "50%",
	left: "50%",
	transform: "translate(-50%, -50%)",
	padding: theme.spacing(2),
}));

const CloseButton = styled(IconButton)(() => ({
	position: "absolute",
	top: -40,
	right: -40,
}));

export const ModalBase = ({ open = false, onClose, children }: Props) => {

	const handleClose = useCallback(() => {
		if (onClose) {
			onClose();
		}
	}, [onClose]);

	return <Modal open={open} onClose={handleClose}>
		<ModalBasePaper>
			<CloseButton onClick={handleClose} color={"primary"}>
				<CloseIcon />
			</CloseButton>
			{children}
		</ModalBasePaper>
	</Modal>;
};