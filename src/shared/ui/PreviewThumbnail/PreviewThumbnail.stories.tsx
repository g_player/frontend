import {PreviewThumbnail} from "@shared/ui";
import {Meta, StoryObj} from "@storybook/react";
import {fn} from "@storybook/test";

const meta: Meta<typeof PreviewThumbnail> = {
    title: "Atoms/PreviewThumbnail",
    component: PreviewThumbnail
}

export default meta;

type Story = StoryObj<typeof meta>

export const Episode: Story = {
    args: {
        src: "https://c4.wallpaperflare.com/wallpaper/682/798/336/movies-fight-club-edward-norton-screenshots-1920x1080-entertainment-movies-hd-art-wallpaper-preview.jpg",
        episodeNumber: 10,
        seasonNumber: 3,
        name: "Залупа Иваныча",
        durationSeconds: 500,
        watchSeconds: 30,
        onClick: fn()
    }
}