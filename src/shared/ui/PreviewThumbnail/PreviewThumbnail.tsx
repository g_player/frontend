import {Typography, useTheme} from "@mui/material";
import {RouteInstance, RouteParams} from "atomic-router";
import {Link} from "atomic-router-react";
import classNames from "classnames";
import {useCallback} from "react";
import s from './styles.module.scss'

interface PreviewThumbnailProps {
    src: string
    onClick?: VoidFunction
    href?: string | RouteInstance<any>
    hrefParams?: RouteParams
    watchSeconds?: number
    durationSeconds?: number
    name: string
    episodeNumber?: number
    seasonNumber?: number
    size?: "small" | "medium" | "large"
}

const PreviewTemplate = (
    {
        src,
        episodeNumber,
        seasonNumber,
        name,
        watchSeconds,
        durationSeconds,
        size = "medium",
        clickable
    }: PreviewThumbnailProps & { clickable?: boolean }) => {

    const theme = useTheme()

    const previewTemplateClassName = classNames(
        s.previewTemplate,
        {
            [s.small]: size === "small",
            [s.medium]: size === "medium",
            [s.large]: size === "large",
            [s.clickable]: clickable
        }
    )

    const getWatchPercent = useCallback(() => {
        if (watchSeconds && durationSeconds) {
            return Math.floor((watchSeconds / durationSeconds) * 100)
        }
        return 0
    }, [watchSeconds, durationSeconds])

    return <div className={previewTemplateClassName} style={{background: theme.palette.background.default}}>
        <img src={src} alt={"preview template"}/>
        {watchSeconds && durationSeconds &&
            <div
                className={s.progress}
                style={{width: `${getWatchPercent()}%`, background: theme.palette.primary.dark}}
            />}
        <div className={s.info}>
            <Typography className={s.name}>{name}</Typography>
            {seasonNumber && episodeNumber &&
                <Typography>
                    Сезон {seasonNumber} Эпизод {episodeNumber}
                </Typography>
            }
        </div>
    </div>
}

export const PreviewThumbnail = (
    {
        href,
        hrefParams,
        onClick,
        ...props
    }: PreviewThumbnailProps) => {
    if (href) {
        return <Link to={href} params={hrefParams} style={{textDecoration: "none"}}>
            <PreviewTemplate clickable={Boolean(href) || Boolean(onClick)} {...props}/>
        </Link>
    }

    return <span>
        <PreviewTemplate clickable={Boolean(href) || Boolean(onClick)} {...props}/>
    </span>
}