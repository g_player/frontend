import {Meta, StoryObj} from "@storybook/react";
import {ComponentProps, useState} from "react";
import {Select} from "./Select";

const SelectWrapper = (props: Omit<ComponentProps<typeof Select>, "onChange">) => {
    const [val, setVal] = useState<string | undefined>(undefined);
    return <Select onChange={(val) => setVal(val)} value={val} {...props}/>
}

const meta: Meta<typeof SelectWrapper> = {
    title: "Base/Select",
    component: SelectWrapper
}

export default meta;

type Story = StoryObj<typeof meta>


export const Default: Story = {
    args: {
        options: [
            {label: "opt1", value: "opt1"},
            {label: "opt2", value: "opt2"},
            {label: "opt3", value: "opt3"},
            {label: "opt4", value: "opt4"},
            {label: "opt5", value: "opt5"}
        ],
        placeholder: "Default select",
    }
}

export const Search: Story = {
    args: {
        options: [
            {label: "opt1", value: "opt1"},
            {label: "opt2", value: "opt2"},
            {label: "opt3", value: "opt3"},
            {label: "opt4", value: "opt4"},
            {label: "opt5", value: "opt5"}
        ],
        placeholder: "Search select",
        search: true
    }
}