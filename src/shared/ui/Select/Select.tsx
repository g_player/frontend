import {TextField} from "@shared/ui";
import classNames from "classnames";
import {ChangeEvent, useCallback, useEffect, useRef, useState} from "react";

type Option = { label: string; value: string };

interface SelectProps {
    placeholder?: string;
    value?: string
    onChange?: (value?: string) => void;
    options?: Option[];
    search?: boolean
    fullWidth?: boolean;
}

export const Select = ({value, onChange, placeholder, options, search, fullWidth}: SelectProps) => {
    const selectRef = useRef<HTMLDivElement>(null);
    const [selected, setSelected] = useState(options?.find(opt => opt.value === value));
    const [inputValue, setInputValue] = useState(selected?.label || "");
    const [isSearching, setIsSearching] = useState(false);
    const [availableOptions, setAvailableOptions] = useState(options);
    const [selectOpen, setSelectOpen] = useState(false);
    const [focused, setFocused] = useState(false);


    const handleInputChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        if (isSearching) {
            setAvailableOptions(options?.filter(opt => opt.label.includes(value, 0)))
            setInputValue(value)
        }
    }, [isSearching, options])

    const handleClickGlobal = useCallback((e: MouseEvent) => {
        if (selectRef.current && !selectRef.current.contains(e.target as HTMLDivElement)) {
            setSelectOpen(false)
            setIsSearching(false)
            setInputValue(selected?.label ?? "")
            setFocused(false)
        }
    }, [selected])

    const handleSelect = useCallback((option: Option) => {
        if (option.value !== selected?.value) {
            setSelectOpen(false)
            setIsSearching(false)
            setSelected(option)
            setInputValue(options?.find(opt => opt.value === option.value)?.label ?? "")
            setFocused(false)
            if (onChange) {
                onChange(option.value)
            }
        }
    }, [onChange, options, selected])

    const handleInputStart = useCallback(() => {
        if (search) {
            setIsSearching(true)
        }
    }, [search])

    const handleOpen = useCallback(() => {
        if (search) {
            setAvailableOptions(options)
        } else {
            setFocused(true)
        }
        setSelectOpen(true)
    }, [search, options])

    useEffect(() => {
        document.addEventListener("click", handleClickGlobal)
        return () => {
            removeEventListener("click", handleClickGlobal)
        }
    }, [handleClickGlobal]);

    return <div ref={selectRef} className="relative">
        {search && <TextField
            onClick={handleOpen}
            type="text"
            placeholder={placeholder}
            onKeyDown={handleInputStart}
            onChange={handleInputChange}
            value={inputValue}
            fullWidth={fullWidth}
        />}
        {!search && <div
            className={classNames(
                "flex justify-between border rounded transition p-3 min-h-12 cursor-pointer bg-white",
                {
                    ["border-g-300 dark:border-g-400"]: !focused,
                    ["border-pc-200 dark:border-pc-400"]: focused,
                    ["w-full"]: fullWidth,
                    ["text-g-500"]: !selected
                }
            )}
            onClick={handleOpen}
        >
            {selected?.label ?? placeholder}
        </div>}
        <div
            className={classNames(
                "transition bg-white absolute w-full mt-1 rounded shadow-lg pt-1 pb-1",
                {
                    ["opacity-0 scale-75 pointer-events-none"]: !selectOpen || (!availableOptions || availableOptions.length === 0),
                    ["opacity-100 scale-100"]: selectOpen && availableOptions && availableOptions.length > 0,
                }
            )}>
            {availableOptions && availableOptions.map((opt) =>
                <div
                    className={classNames(
                        "p-3 hover:bg-g-100 cursor-pointer transition",
                        {
                            ["bg-pc-100 rounded-none hover:bg-pc-100 cursor-default"]: selected && selected.value === opt.value,
                        }
                    )}
                    onClick={() => handleSelect(opt)}
                    key={opt.value}
                >
                    {opt.label}
                </div>
            )}
        </div>
    </div>
}