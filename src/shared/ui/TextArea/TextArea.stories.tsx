import {Meta, StoryObj} from "@storybook/react";
import {TextArea} from "./TextArea";

const meta: Meta<typeof TextArea> = {
    title: "Base/TextArea",
    component: TextArea,
}

export default meta;
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        placeholder: "Default"
    }
}

export const FullWidth: Story = {
    args: {
        placeholder: "Full width",
        fullWidth: true
    }
}


export const Error: Story = {
    args: {
        placeholder: "Error",
        hasError: true,
        helperText: "error text"
    }
}