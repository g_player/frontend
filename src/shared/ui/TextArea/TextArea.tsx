import classNames from "classnames";
import {InputHTMLAttributes} from "react";

interface TextAreaProps extends InputHTMLAttributes<HTMLTextAreaElement> {
    fullWidth?: boolean;
    hasError?: boolean;
    helperText?: string;
}

export const TextArea = ({fullWidth, hasError, helperText, className, ...props}: TextAreaProps) => {
    return <div className={classNames({["w-full"]: fullWidth})}>
        <textarea className={
            classNames(
                "rounded border resize-none p-3 transition outline-none",
                {
                    ["border-g-300 focus:border-pc-200"]: !hasError,
                    ["border-r-500 text-r-500"]: hasError,
                    ["w-full"]: fullWidth,
                    [className!]: !!className,
                })
        } {...props}/>
        <div className={classNames(
            "pl-3 text-sm",
            {
                ["border-r-500 text-r-500"]: hasError,
            }
        )}>
            {helperText}
        </div>
    </div>
}