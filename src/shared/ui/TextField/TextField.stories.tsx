import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import {IconButton} from "@shared/ui";
import {TextField} from "@shared/ui/TextField/TextField.tsx";
import {Meta, StoryObj} from "@storybook/react";

const meta: Meta<typeof TextField> = {
    title: "Base/TextField",
    component: TextField,
}

export default meta;
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        placeholder: "Default"
    }
}

export const WithIcon: Story = {
    args: {
        placeholder: "With Icon",
        type: "password",
        endAdornment: <IconButton><VisibilityOffIcon/></IconButton>
    }
}

export const Error: Story = {
    args: {
        placeholder: "Error",
        hasError: true,
        helperText: "Error text"
    }
}