import classNames from "classnames";
import {InputHTMLAttributes, ReactNode, useRef, useState} from "react";

interface TextFieldProps extends InputHTMLAttributes<HTMLInputElement> {
    fullWidth?: boolean;
    endAdornment?: ReactNode;
    hasError?: boolean;
    helperText?: string;
}

export const TextField = ({type = "text", fullWidth, endAdornment, hasError, helperText, ...props}: TextFieldProps) => {
    const [focused, setFocused] = useState(false);
    const inputRef = useRef<HTMLInputElement>(null);

    return <div className={classNames({["w-full"]: fullWidth})}>
        <div className={classNames(
            "flex justify-between border rounded transition bg-white dark:bg-g-800",
            {
                ["border-g-300 dark:border-g-400"]: !focused && !hasError,
                ["border-c-200 dark:border-c-400"]: focused && !hasError,
                ["border-r-500 text-r-500"]: hasError,
                ["w-full"]: fullWidth,
            }
        )}>
            <input
                ref={inputRef}
                type={type}
                className={classNames(
                    "grow p-3 bg-transparent outline-none dark:text-white transition",
                    {
                        [" w-full"]: fullWidth
                    }
                )}
                onFocus={() => setFocused(true)}
                onBlur={() => setFocused(false)}
                {...props}
            />
            {endAdornment && <div>{endAdornment}</div>}
        </div>
        {helperText && <div className={classNames(
            "pl-3 pt-3 text-sm",
            {
                ["border-r-500 text-r-500"]: hasError
            }
        )}>
            {helperText}
        </div>}
    </div>
}