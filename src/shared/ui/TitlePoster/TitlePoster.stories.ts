import type { Meta, StoryObj } from "@storybook/react";
import { fn } from "@storybook/test";
import { TitlePoster } from "./TitlePoster.tsx";

const meta: Meta<typeof TitlePoster> = {
	title: "Atoms/TitlePoster",
	component: TitlePoster,
};

export default meta;

type Story = StoryObj<typeof meta>


export const Default: Story = {
	args: {
		url: "https://cdn.europosters.eu/image/750/posters/pulp-fiction-cover-i1288.jpg",
		onClick: fn(),
		size: "medium",
	},
};

export const Disabled: Story = {
	args: {
		url: "https://cdn.europosters.eu/image/750/posters/pulp-fiction-cover-i1288.jpg",
		onClick: fn(),
		size: "medium",
		disabled: true,
	},
};