import { RouteInstance, RouteParams } from "atomic-router";
import { Link } from "atomic-router-react";
import classNames from "classnames";
import { useCallback, useMemo } from "react";
import s from "./styles.module.scss";

interface TitlePosterProps {
	url: string;
	onClick?: () => void;
	href?: string | RouteInstance<any>;
	disabled?: boolean;
	size?: "small" | "medium" | "large";
	hrefParams?: RouteParams;
}

interface ImageProps {
	url: string;
}

const Image = ({ url }: ImageProps) => {
	return <img src={url} alt={"poster image"} />;
};

export const TitlePoster = ({ url, onClick, disabled, href, size = "medium", hrefParams }: TitlePosterProps) => {

	const titlePosterClassName = useMemo(() => classNames(s.titlePoster, {
		[s.disabled]: disabled,
		[s.small]: size === "small",
		[s.medium]: size === "medium",
		[s.large]: size === "large",
		[s.animated]: onClick || href,
	}), [disabled, size, onClick, href]);

	const handleClick = useCallback(() => {
		if (onClick) {
			onClick();
		}
	}, [onClick]);

	if (href) {
		return <Link to={href} params={hrefParams} className={titlePosterClassName} onClick={handleClick}>
			<Image url={url} />
		</Link>;
	}

	return <span className={titlePosterClassName} onClick={handleClick}>
			<Image url={url} />
		</span>;
};