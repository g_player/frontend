import {$downloadsTrackList, closeDownloadsSocket, openDownloadsSocket} from "@entities/downloads";
import {api, DownloadsSettings} from "@shared/api";
import {attach, createEvent, createStore, sample} from "effector";
import {createGate} from "effector-react";
import {reset} from "patronum";

export const Gate = createGate();

const getDownloadsFx = attach({effect: api.downloadsSettings.getDownloadsFx})
const createTorrentByFileFx = attach({effect: api.downloadsSettings.createTorrentByFileFx})
const pauseDownloadFx = attach({
    effect: api.downloadsSettings.controlTorrentFx,
    mapParams: (id: number) => ({id, action: DownloadsSettings.TorrentControlAction.PAUSE})
})
const continueDownloadFx = attach({
    effect: api.downloadsSettings.controlTorrentFx,
    mapParams: (id: number) => ({id, action: DownloadsSettings.TorrentControlAction.CONTINUE})
})
const deleteTorrentWithFileFx = attach({
    effect: api.downloadsSettings.controlTorrentFx,
    mapParams: (id: number) => ({id: id, action: DownloadsSettings.TorrentControlAction.DELETE_WITH_FILES})
})
const deleteTorrentOnlyFx = attach({
    effect: api.downloadsSettings.controlTorrentFx,
    mapParams: (id: number) => ({id: id, action: DownloadsSettings.TorrentControlAction.DELETE}),
})

export const downloadFocused = createEvent<DownloadsSettings.DownloadDTO>()
export const selectedDownloadPaused = createEvent()
export const selectedDownloadContinued = createEvent()
export const addedDownload = createEvent()
export const newDownloadChanged = createEvent<File>()
export const newDownloadCleared = createEvent()
export const newDownloadCreated = createEvent()
export const torrentDeleteStarted = createEvent()
export const deleteTorrentModalClosed = createEvent()
export const torrentWithFileDeleted = createEvent()
export const onlyTorrentDeleted = createEvent()
export const downloadBlur = createEvent()

export const $downloads = createStore<DownloadsSettings.DownloadDTO[]>([])
export const $selectedDownload = createStore<DownloadsSettings.DownloadDTO | null>(null)
export const $idAddingDownload = createStore(false)
export const $newDownload = createStore<File | null>(null)
export const $isDeleteModalOpened = createStore(false)

$selectedDownload.on(downloadFocused, (_, focused) => focused)
$selectedDownload.on(downloadBlur, () => null)

$idAddingDownload.on(addedDownload, () => true)

$newDownload.on(newDownloadChanged, (_, file) => file)
$newDownload.on(newDownloadCleared, () => null)

$isDeleteModalOpened.on(torrentDeleteStarted, () => true)
$isDeleteModalOpened.on(deleteTorrentModalClosed, () => false)


reset({
    clock: Gate.close,
    target: [$downloads, $selectedDownload, $idAddingDownload, $newDownload, $isDeleteModalOpened]
})

reset({
    clock: [deleteTorrentOnlyFx.doneData, deleteTorrentWithFileFx.doneData],
    target: [$isDeleteModalOpened, $selectedDownload]
})

sample({
    source: getDownloadsFx.doneData,
    fn: (res) => res.downloads,
    target: $downloads
})

sample({
    clock: Gate.open,
    target: [openDownloadsSocket, getDownloadsFx],
})

sample({
    clock: Gate.close,
    target: closeDownloadsSocket
})

sample({
    source: $downloadsTrackList,
    filter: (downloads) => !!downloads,
    fn: (downloads) => downloads!,
    target: $downloads
})


sample({
    clock: newDownloadCreated,
    source: $newDownload,
    filter: (download) => !!download,
    fn: (download) => ({file: download!}),
    target: createTorrentByFileFx
})

sample({
    clock: selectedDownloadPaused,
    source: $selectedDownload,
    filter: (selectedDownload) => selectedDownload !== null,
    fn: (selectedDownload) => selectedDownload!.id,
    target: pauseDownloadFx
})

sample({
    clock: selectedDownloadContinued,
    source: $selectedDownload,
    filter: (selectedDownload) => selectedDownload !== null,
    fn: (selectedDownload) => selectedDownload!.id,
    target: continueDownloadFx
})

sample({
    clock: torrentWithFileDeleted,
    source: $selectedDownload,
    filter: selected => !!selected,
    fn: selected => selected!.id,
    target: deleteTorrentWithFileFx
})

sample({
    clock: onlyTorrentDeleted,
    source: $selectedDownload,
    filter: selected => !!selected,
    fn: selected => selected!.id,
    target: deleteTorrentOnlyFx
})

sample({
    clock: pauseDownloadFx.doneData,
    source: $selectedDownload,
    filter: selected => !!selected,
    fn: (selected): DownloadsSettings.DownloadDTO => ({...selected!, status: "paused"}),
    target: $selectedDownload
})

sample({
    clock: continueDownloadFx.doneData,
    source: $selectedDownload,
    filter: selected => !!selected,
    fn: (selected): DownloadsSettings.DownloadDTO => ({...selected!, status: "loading"}),
    target: $selectedDownload
})