import DeleteIcon from '@mui/icons-material/Delete';
import PauseIcon from '@mui/icons-material/Pause';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import {Button, IconButton, Modal} from "@shared/ui";
import {DropArea} from "@shared/ui/DropArea";
import classNames from "classnames";
import {useGate, useUnit} from "effector-react";
import {useCallback, useEffect, useRef} from "react";
import {
    $downloads,
    $idAddingDownload,
    $isDeleteModalOpened,
    $newDownload,
    $selectedDownload,
    addedDownload,
    deleteTorrentModalClosed,
    downloadBlur,
    downloadFocused,
    Gate,
    newDownloadChanged,
    newDownloadCleared,
    newDownloadCreated,
    onlyTorrentDeleted,
    selectedDownloadContinued,
    selectedDownloadPaused,
    torrentDeleteStarted,
    torrentWithFileDeleted
} from '../../model'

export const DownloadPanel = () => {
    useGate(Gate)
    const [
        downloads,
        selectedDownload,
        isAddingDownload,
        newDownload,
        isDeleteModalOpened,
    ] = useUnit([
        $downloads,
        $selectedDownload,
        $idAddingDownload,
        $newDownload,
        $isDeleteModalOpened
    ])

    const wrapperRef = useRef<HTMLDivElement>(null)
    const modalRef = useRef<{ isClickTarget: (e: MouseEvent) => boolean | null }>(null)

    const handleClickOutside = useCallback((e: MouseEvent) => {
        const isWrapperClicked = wrapperRef.current && wrapperRef.current.contains(e.target as Element)
        const isModalClicked = modalRef.current && modalRef.current.isClickTarget(e)
        if (selectedDownload && !isModalClicked && !isWrapperClicked) {
            downloadBlur()
        }
    }, [selectedDownload])

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        }
    }, [handleClickOutside]);

    return <>
        <div
            ref={wrapperRef}
            className="bg-white shadow-md rounded-lg p-3"
        >
            <div className="flex content-center justify-between">
                <h3 className="p-3 text-lg">Загрузки</h3>
                {<div className={classNames("transition", {
                    ["opacity-0 "]: !selectedDownload,
                    ["opacity-100"]: !!selectedDownload
                })}>
                    <IconButton
                        disabled={!selectedDownload}
                        onClick={() => selectedDownload?.status === "loading" ? selectedDownloadPaused() : selectedDownloadContinued()}>
                        {selectedDownload && selectedDownload.status === "loading" ? <PauseIcon/> : <PlayArrowIcon/>}
                    </IconButton>
                    <IconButton
                        disabled={!selectedDownload}
                        onClick={() => torrentDeleteStarted()}>
                        <DeleteIcon/>
                    </IconButton>
                </div>
                }
            </div>
            <div className="w-full overflow-auto pb-3">
                <table className="w-full text-center table-auto overflow-scroll">
                    <thead className="border-pc-100 border-b-2">
                    <tr>
                        <td className="p-3">Статус</td>
                        <td className="p-3">Имя</td>
                        <td className="p-3">Прогресс</td>
                        <td className="p-3">Скорость</td>
                    </tr>
                    </thead>
                    <tbody>
                    {downloads.map((item) => <tr
                        key={item.id}
                        className={classNames("transition", {
                            ["hover:bg-pc-200/20 cursor-pointer"]: selectedDownload?.id !== item.id,
                            ["bg-pc-100"]: selectedDownload?.id === item.id,
                        })}
                        onClick={() => downloadFocused(item)}
                    >
                        <td className="p-3">{item.status}</td>
                        <td className="p-3">{item.name}</td>
                        <td className="p-3">{item.completion_percentage}</td>
                        <td className="p-3">{item.speed}</td>
                    </tr>)}
                    </tbody>
                </table>
            </div>
            <div className="mt-3">
                {!isAddingDownload && <Button
                    className="mt-3"
                    fullWidth
                    variant="contained"
                    onClick={() => addedDownload()}
                >
                    Добавить
                </Button>
                }
                {isAddingDownload && !newDownload && <DropArea onChange={(file) => newDownloadChanged(file as File)}/>}
                {isAddingDownload && newDownload && <div>
                    <div className="p-3">{newDownload.name}</div>
                    <div className="flex gap-3">
                        <Button variant="outlined" fullWidth onClick={() => newDownloadCleared()}>Очистить</Button>
                        <Button variant="contained" fullWidth onClick={() => newDownloadCreated()}>Добавить</Button>
                    </div>
                </div>}
            </div>
        </div>
        <Modal ref={modalRef} opened={isDeleteModalOpened} onClose={() => deleteTorrentModalClosed()} withCloseButton>
            <div className="max-w-80 flex flex-col break-all">
                Удалить торрент
                <p>"{selectedDownload?.name}"?</p>
                <div className="flex gap-3 mt-3 self-end break-normal">
                    <Button variant="contained" onClick={() => onlyTorrentDeleted()}>
                        Удалить торрент
                    </Button>
                    <Button variant="outlined" onClick={() => torrentWithFileDeleted()}>
                        Удалить вместе с файлом
                    </Button>
                </div>
            </div>
        </Modal>
    </>
}