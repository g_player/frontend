import {$loggedInUserInfo, logout} from "@app/session.ts";
import {routes} from "@shared/routing";
import {Header} from "@shared/ui";
import {useUnit} from "effector-react";
import {ReactNode} from "react";

interface MainLayoutProps {
    children?: ReactNode;
}

export const MainLayout = ({children}: MainLayoutProps) => {
    const [userInfo] = useUnit([$loggedInUserInfo]);

    return <div>
        <Header
            showSettings={userInfo?.role === "admin"}
            settingsButtonRoute={routes.settings.dashboard}
            onLogoutButtonClick={logout}
        />
        <div>
            {children}
        </div>
    </div>;
};