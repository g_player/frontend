import {logout} from "@app/session.ts";
import {routes} from "@shared/routing";
import {Header} from "@shared/ui";
import {ReactNode} from "react";

interface SettingsLayoutProps {
    children?: ReactNode;
}

export const SettingsLayout = ({children}: SettingsLayoutProps) => {


    return <div className="flex flex-col h-full">
        <Header showSettings settingsButtonRoute={routes.main.dashboard} onLogoutButtonClick={logout}/>
        <div className="flex h-full">
            <div className="flex flex-col w-11/12">
                <div className="p-3">
                    {children}
                </div>
            </div>
        </div>
    </div>
};