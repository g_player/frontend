import { createEvent, createStore } from "effector";

export type SettingsTabs = "users" | "titles" | "torrents"

export const settingsTabChanged = createEvent<SettingsTabs>();

export const $currentSettingsTab = createStore<SettingsTabs>("users");