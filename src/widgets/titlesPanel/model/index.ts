import {addTitleModalClosed, createTitleFx} from "@features/addTitle";
import {api, Title} from "@shared/api";
import {attach, createEvent, createStore, sample} from "effector";
import {createGate} from "effector-react";
import {reset} from "patronum";

const GET_TITLES_COUNT = 10

const getTitlesFx = attach({effect: api.title.getTitlesFx})
const publishTitleFx = attach({
    effect: api.title.controlTitleFx,
    mapParams: (id: number) => ({title_id: id, action: Title.TitleControlAction.PUBLISH})
})
const withdrawTitleFx = attach({
    effect: api.title.controlTitleFx,
    mapParams: (id: number) => ({title_id: id, action: Title.TitleControlAction.WITHDRAW})
})

export const Gate = createGate()

export const titleSelected = createEvent<Title.TitleDTO>()
export const titlePublished = createEvent()
export const titleWithdraw = createEvent()

export const $titles = createStore<Title.TitleDTO[]>([])
export const $titlesOffset = createStore(0)
export const $selectedTitle = createStore<Title.TitleDTO | null>(null)


reset({
    clock: Gate.close,
    target: [$titles, $titlesOffset, $selectedTitle],
})

$selectedTitle.on(titleSelected, (_, title) => title)

sample({
    clock: [Gate.open, createTitleFx.doneData, withdrawTitleFx.doneData, publishTitleFx.doneData],
    source: $titlesOffset,
    fn: (offset) => ({pagination: {offset: offset, count: GET_TITLES_COUNT}}),
    target: getTitlesFx
})

sample({
    source: getTitlesFx.doneData,
    fn: (res) => res.titles,
    target: $titles
})

sample({
    clock: createTitleFx.doneData,
    target: addTitleModalClosed
})

sample({
    clock: titlePublished,
    source: $selectedTitle,
    filter: (title) => title !== null,
    fn: (title) => title!.id,
    target: publishTitleFx
})

sample({
    clock: titleWithdraw,
    source: $selectedTitle,
    filter: (title) => title !== null,
    fn: (title) => title!.id,
    target: withdrawTitleFx
})

sample({
    clock: getTitlesFx.doneData,
    source: {titles: $titles, selectedTitle: $selectedTitle},
    filter: ({selectedTitle}) => selectedTitle !== null,
    fn: ({selectedTitle, titles}) => titles.find(title => title.id === selectedTitle!.id) ?? null,
    target: $selectedTitle
})


