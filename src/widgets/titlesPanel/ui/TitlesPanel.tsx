import {AddTitleModal, addTitleModalShown} from "@features/addTitle";
import AddIcon from '@mui/icons-material/Add';

import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import {IconButton} from "@shared/ui";
import classNames from "classnames";
import {useGate, useUnit} from 'effector-react'
import {$selectedTitle, $titles, Gate, titlePublished, titleSelected, titleWithdraw} from '../model'

export const TitlesPanel = () => {
    useGate(Gate)
    const [titles, selectedTitle] = useUnit([$titles, $selectedTitle])


    return <>
        <div
            className="bg-white shadow-md rounded-lg p-3"
        >
            <div className="flex content-center justify-between">
                <h3 className="p-3 text-lg">Тайтлы</h3>
                <div className="flex">
                    <div className={classNames("transition", {
                        ["opacity-100"]: !!selectedTitle,
                        ["opacity-0"]: !selectedTitle
                    })}>
                        <IconButton
                            disabled={!selectedTitle}
                            onClick={() => selectedTitle?.published ? titleWithdraw() : titlePublished()}
                        >
                            {selectedTitle?.published ? <VisibilityOffIcon/> : <VisibilityIcon/>}
                        </IconButton>
                    </div>
                    <div className="h-[48px] w-[48px]">
                        <IconButton onClick={() => addTitleModalShown()}>
                            <AddIcon/>
                        </IconButton>
                    </div>
                </div>
            </div>
            {titles.length > 0 && <div className="max-h-96 overflow-auto">
                <table className="table-auto text-center">
                    <thead>
                    <tr>
                        <td className="p-3"></td>
                        <td className="p-3">Id</td>
                        <td className="p-3">Название</td>
                        <td className="p-3">Тип</td>
                        <td className="p-3">Виден для пользователей</td>
                    </tr>
                    </thead>
                    <tbody>
                    {titles.map(title => <tr
                        className={classNames("transition", {
                            ["hover:bg-pc-200/20 cursor-pointer"]: title.id !== selectedTitle?.id,
                            ["bg-pc-100"]: title.id === selectedTitle?.id,
                        })}
                        onClick={() => titleSelected(title)}
                        key={title.id}
                    >
                        <td className="p-3 w-1/6">
                            <img
                                alt="title image"
                                src={title.poster_preview_url}
                                className="rounded"
                            />
                        </td>
                        <td className="p-3">{title.id}</td>
                        <td className="p-3">{title.name}</td>
                        <td className="p-3">{title.type}</td>
                        <td className="p-3">{title.published ? "Виден" : "Не виден"}</td>
                    </tr>)}
                    </tbody>
                </table>
            </div>}
        </div>
        <AddTitleModal/>
    </>
}