import {api, UserSettings} from "@shared/api";
import {attach, createStore, sample} from "effector";
import {createGate} from "effector-react";

export const Gate = createGate()

const getUsersFx = attach({effect: api.usersSettings.getUsersFx})

const $getUsersOffset = createStore(0)
export const $users = createStore<UserSettings.UserDTO[] | null>(null)
export const $isUsersLoading = getUsersFx.pending

sample({
    clock: Gate.open,
    source: $getUsersOffset,
    fn: (offset) => ({offset: offset, count: 10}),
    target: getUsersFx,
})

sample({
    source: getUsersFx.doneData,
    fn: (res) => res.users,
    target: $users
})