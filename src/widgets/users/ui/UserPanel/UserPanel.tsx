import {CreateRegistrationTokenModal, modalShown} from '@features/createRegistrationToken'
import AddIcon from '@mui/icons-material/Add';
import {IconButton} from "@shared/ui";
import {useGate, useUnit} from "effector-react";
import {$isUsersLoading, $users, Gate} from '../../model'

export const UserPanel = () => {
    useGate(Gate)
    const [users, isUsersLoading] = useUnit([$users, $isUsersLoading])

    return <div>
        {!users || users.length === 0 && <div>Нет доступных пользователей</div>}
        {isUsersLoading && <div>Loading</div>}
        {users && users.length && !isUsersLoading && <div className="bg-white shadow-md rounded-lg p-3">
            <div className="flex items-center justify-between">
                <h3 className="p-3 text-lg">Пользователи</h3>
                <IconButton onClick={() => modalShown()}>
                    <AddIcon/>
                </IconButton>
            </div>
            <table className="table-auto w-full text-center">
                <thead>
                <tr className="p-3">
                    <td className="p-3">id</td>
                    <td className="p-3">Имя пользователя</td>
                    <td className="p-3">Роль</td>
                </tr>
                </thead>
                <tbody>
                {users.map(user => <tr className="p-3" key={user.id}>
                    <td className="p-3">{user.id}</td>
                    <td className="p-3">{user.username}</td>
                    <td className="p-3">{user.role}</td>
                </tr>)}
                </tbody>
            </table>
        </div>}
        <CreateRegistrationTokenModal/>
    </div>
}