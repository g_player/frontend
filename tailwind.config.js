/** @type {import('tailwindcss').Config} */
export default {
    darkMode: "selector",
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
        "./.storybook/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {
            animation: {
                "smooth-animation": "0.3s cubic-bezier(.25, .8, .25, 1)",
                "slide-to-right": "slide-to-right 0.3s cubic-bezier(.25, .8, .25, 1)"
            },
            keyframes: {
                "slide-from-right": {
                    '0%': {transform: 'translateX(9999px)'},
                    '100%': {transform: 'translateX(0px)'},
                },
                "slide-to-right": {
                    '0%': {transform: 'translateX(0px)'},
                    '100%': {transform: 'translateX(9999px)'},
                }
            },
            colors: {
                //primary-color
                "pc-100": "#B2EBF2",
                "pc-200": "#80DEEA",
                "pc-300": "#4DD0E1",
                "pc-400": "#26C6DA",
                "pc-500": "#00BCD4",
                "pc-600": "#00ACC1",
                //cyan
                "c-100": "#B2EBF2",
                "c-200": "#80DEEA",
                "c-300": "#4DD0E1",
                "c-400": "#26C6DA",
                "c-500": "#00BCD4",
                "c-600": "#00ACC1",
                //gray
                "g-100": "#F5F5F5",
                "g-200": "#EEEEEE",
                "g-300": "#E0E0E0",
                "g-400": "#BDBDBD",
                "g-500": "#9E9E9E",
                "g-600": "#757575",
                "g-700": "#616161",
                "g-800": "#424242",
                //red
                "r-100": "#FFCDD2",
                "r-200": "#EF9A9A",
                "r-300": "#E57373",
                "r-400": "#EF5350",
                "r-500": "#F44336",
                "r-600": "#E53935",
            }
        },
    },
    plugins: [],
}

